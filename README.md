# WatDOS

Using QEMU and GDB:  

```
qemu-system-i386 --cpu SandyBridge -S -s -hda /home/mgreene/Development/bmach/PCDOS70Dev.vdi

```
Running screen captures:   
![alt text](images/bochs.png "Bochs Machine")
![alt text](images/qemu.png "Qemu Machine")
