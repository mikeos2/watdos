


MBR --> loads BPB at 0x7C00


BPB --> loads watload at 0xC000 use 0xD000 and up as working buffer
watload:
  1 - Display watload running
  2 - Move FAT BPB data to this code section
  3 - Load root directory and find watbios.bin
  4 - Load watbios.bin [loacation 0x1000]
  5 - Enable/check A20
  6 - Check CPU/emulator greater than 286
  7 - jmp watbios start

Note: Number 2 - do a complete copy of BPB from bpb to watload



	|--------| 
	|watload | 
	|buffer  | 
	|--------| 0xD000 
	|        | 
	|watload | at 0xC000 (about 0x578 [1.4k] as of April 2019 
	|        | try to keep under 0x800 bytes - I removed padding)
	+--------+
	|        | 
	| Buffer | Use start 0x7E00 for Root Dir and FAT load buffer
	|--------| 0000:7E00  (0:BP+200)
	|BOOT SEC| contains BPB
	|ORIGIN  |
	|--------| 0000:7C00  (0:BP)
	|STACK   | minimal for bpb code
	|- - - - |
	|        |  
	|        |  
	|--------| 0070:0000 (0:0700)
	|        | 
	|        | 
	|--------| 0000:0600 
	|        | 
	|        | Cluster list temp from FAT read for BPB
	|********| 0000:0500
	|BDA     | BIOS Data Area
	+--------+ 0000:0400
	|IVT     | Interrupt Vector Table
	+--------+ 0000:0000
	







	|--------| 
	|watload | 
	|buffer  | 
	|--------| 0xD000 
	|        | 
	|watload | at 0xC000 (about 0x578 [1.4k] as of April 2019 
	|        | try to keep under 0x800 bytes - I removed padding)
	+--------+
	|        | 
	|        |
	|        |
	|        |
	|        |
	|        |
	|        | 
	|        |  
	|watbios |  
	|--------| 0x1000
	|        | 
	|--------| 0000:0600 
	|        | 
	|        | 
	|********| 0000:0500
	|BDA     | BIOS Data Area
	+--------+ 0000:0400
	|IVT     | Interrupt Vector Table
	+--------+ 0000:0000

