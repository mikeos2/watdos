;****************************************************************************
; *
; *  watload.asm -- Development Loader
; *
; *  ========================================================================
; *
; *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
; *                      March-July 2019
; *
; *  ========================================================================
; *
; *  Description: Loaded at 0xC000 by the BPB. Loads BIOS to 0x10000
; *  and jmps to start 0x10000 in pmode.
; *
; *  A lot of this is rehash from the BPB, but much more room to play in.
; *
; *  Assume the registers are trash at start, maybe I'll pass something
; *  usful later to save time here.
; *
; *  1 - Display watload running
; *  2 - Move FAT BPB data to this code section
; *  3 - Store GDT for pmode at 0x0800 <-- set by GDTSeg:GDTOff
; *  4 - Load root directory and find watbios.bin
; *  5 - Load watbios.bin
; *  6 - Enable/check A20 - A20 must be on
; *  7 - Check CPU/emulator greater than 286
; *  8 - load dummy idt and switch to pmode 
; *  9 - jmp far to bios start
; *
; *  ========================================================================
; *
; *   This program is free software; you can redistribute it and/or modify
; *   it under the terms of the GNU General Public License as published by
; *   the Free Software Foundation; either version 2 of the License, or
; *   (at your option) any later version.
; *
; *   This program is distributed in the hope that it will be useful,
; *   but WITHOUT ANY WARRANTY; without even the implied warranty of
; *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; *   GNU General Public License for more details.
; *
; *   You should have received a copy of the GNU General Public License
; *   along with this program; if not, write to the Free Software
; *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
; *
; ***************************************************************************
.8086

; span   0xC000 - 0xC7FF 2048 bytes
; stack  0xC000 - Down
; buffer 0xD000 - up 

; Filesystem structures
include bpbdef.inc           
include daptable.inc
include timer.inc

; Real mode data structure
include rmdata.inc

BASEADDR    equ	   0xC000       ; load address of watload.bin

BufferSeg   equ    0x0000       ; segment/offset for buffer
BufferOff   equ    0xD000

BIOSSeg     equ    0x1000       ; segment/offset for BIOS load
BIOSOff     equ    0x0000       ; 0x10000
BIOSLin     equ    ((BIOSSeg * 0xF) + BIOSOff)

MEMMapSeg   equ    0x0000       ; segment/offset for memory map
MEMMapOff   equ    0x0900
MEMMapLin   equ    ((MEMMapSeg * 0xF) + MEMMapOff)

; GDT Location - per Intel: aligned on an eight-byte boundary to yield 
; the best processor performance
GDTSeg      equ    0x0000       ; segment/offset for pmode GDT
GDTOff      equ    0x0800
GDTLin      equ    ((GDTSeg * 0xF) + GDTOff)

; Dummy Interrupt Descriptor Table (IDT) location
; Set size to 2048 (256 byes * 8 bytes per entry) reserve full table
; with GDT at 0x0800 the IDT will be right behind it
IDTSize     equ    0x800
IDTLoc      equ    0x000000000

TMPDELAYNUM equ    0x0400

; Default layout at pmode jump to 0x10000
;
; |--------| 
; |watbios | 
; |        | 
; |--------| 0x10000  Watload places watbios here
; |        |
; ~        ~
; |        |
; |--------| 
; |watload | 
; |buffer  | 
; |--------| 0xD000 
; |        | 
; |watload | at 0xC000 (about 0x578 [1.4k] as of April 2019 
; |        | try to keep under 0x800 bytes - I removed padding)
; |--------| 0000:C000 
; |        | 
; ~        ~  approx 46336 bytes free here
; |        | 
; |--------| 0000:0B00
; |Mem Map |      I am estimating 512 bytes for memmap
; |--------| 0000:0900 
; |  GDT   | 
; |********| 0000:0800
; |        | 
; |        |
; |  IDT   | Interrupt Descriptor Table
; +--------+ 0000:0000
	
; Start of watload segment
_TEXT segment dword public use16 'CODE'
            org     0xC000
          
; ********************************************************************
; Entry from BPB
; ********************************************************************

loader_start:   

; ********************************************************************
; * Start of watload code
; *
; * Boot sector passes in cx, dx, si, di
; ********************************************************************

    mov    word ptr [fat_start], cx     ; fat_start
    mov    word ptr [fat_start+2], dx   ; fat_start+2 
    mov    word ptr data_start, si      ; data_start
    mov    word ptr data_start+2, di    ; data_start+2

    cli                  ; stop interupts
    cld                  ; strings read up
    xor    ax, ax              
    mov    ds, ax        ; set seg regs
    mov    es, ax
    mov    ss, ax
    mov    bp, BASEADDR  ; setup stack
    mov    sp, BASEADDR
    sti                  ; interupts on
    
    ; clear screen and display message
    call   LoadMSG
   
    ; move data from loaded BPB at 0x7C00
    call   MoveBPB
    
    ; so here is what we will do, read in test the root directory again
    ; the following is duplicate of the BPB - we pulled in fat_start
    ; from BPB data
    mov    si, word ptr [fat_start]
    mov    di, word ptr [fat_start+2]  ; DI:SI = first FAT sector

    ; if I have space, better to do and store in the BPB and copy
    ; up with other data, but I have the space here and time
    ; dir_start = (BPB_NumFATs * BPB_FATSz16) + fat_start
    mov    al, BPBTable.BPB_NumFATs
    cbw    
    mul    BPBTable.BPB_FATSz16           ; DX:AX = total number of FAT sectors
    add    si, ax
    adc    di, dx                         ; DI:SI = first root directory sector
    mov	   word ptr DAP.DAP_sector_low, si		
    mov	   word ptr DAP.DAP_sector_low+2, di  ; root dir start in DAP will be first read in

    ; RootDirSectors = (BPB_RootEntCnt * 32) / BPB_BytsPerSec;
    mov    ax, 32
    xor    dx, dx
    mov    bx, [BPBTable.BPB_RootEntCnt]
    mul    word ptr [BPBTable.BPB_RootEntCnt]
    div    word ptr [BPBTable.BPB_BytsPerSec]  ; Divide (dx:ax,sectsize) to (ax,dx)
    mov    DAP.DAP_num_sectors, ax             ; number of root dir sectors to DAP
    mov    DAP.DAP_buffer_seg, BufferSeg
    mov    DAP.DAP_buffer_off, BufferOff
    call   ReadDrive                           ; read in root directory
    jnc	   get_biosname  
    
    ; Error 2      
    mov    dx, offset [error2] 
    call   PrintMsg                            ; Error 2 - Root directory read
    jmp $
      
    ; ******  read through root dir directory for bios  ****** 
get_biosname:
    lea    si, filename                 ; filename
    mov    ax, BufferSeg                ; setup buffer for 0xD000
    mov    es, ax
    mov    di, BufferOff
    mov	   ax, BPBTable.BPB_RootEntCnt  ; Count FAT 16 directory entries max

    ; this loop read through root directory to find target file
    ; not sure if si needs to be saved - di does, cmpsb inc so
    ; throws off count if not    
next_dir:
    mov	   cx, 0x0B            ; Scanning 11 bytes (CX is used by REPE)
    push   si                  ; save di/si values - cmpsb changes both
    push   di
    repe   cmpsb               ; ...and compare it. Case sensitive!
    je     bios_found 
    pop	   di                  ; restore di/si and bump di to next
    add	   di, 0x20            ; directory entry +0x20
    pop	   si                   
    cmp    byte ptr [es:di], 0x00
    jz	   no_bios	           ; ax counts max dir entries, but if first 
    dec	   ax                  ; char di is zero, no more dir entries
    jne	   next_dir            ; so bail early
    ; ****** loop bottom

    ; Error 3    
no_bios:
    mov    dx, offset [error3]
    call   PrintMsg            ; Error 3 - watlbios.bin not found
    jmp $

    ; Info Msg 1
bios_found:
    mov    dx, offset [info1]  ; Root directory read and bios found
    call   PrintMsg

    pop    di                  ; offset off stack - dir entry for bios 
    pop    si                  ; empty stack

    ; directory entry found, pull cluster and file size, store in data
    ; cluster/size info at di + the offset:
    ;
    ; 26-27   Starting cluster (0 for an empty file)
    ; 28-31   Filesize in bytes
    ;
    ; 00h	8 bytes	Filename
    ; 08h	3 bytes	Filename Extension
    ; 0Bh	1 bytes	Attribute Byte
    ; 0Ch	1 bytes	Reserved for Windows NT
    ; 0Dh	1 bytes	Creation - Millisecond stamp (actual 100th of a second)
    ; 0Eh	2 bytes	Creation Time
    ; 10h	2 bytes	Creation Date
    ; 12h	2 bytes	Last Access Date
    ; 14h	2 bytes	Reserved for FAT32
    ; 16h	2 bytes	Last Write Time
    ; 18h	2 bytes	Last Write Date
    ; 1Ah	2 bytes	Starting cluster   <---
    ; 1Ch	4 bytes	File size in bytes
    
    push   ds
    mov    ax, BufferSeg       ; set for read cluster - yeah, could be 
    mov    ds, ax              ; better but I have room
    mov    ax, [di+0x1C]
    mov    word ptr [bios_size], ax
    mov    ax, [di+0x1E]
    mov    word ptr [bios_size+2], ax	
    mov    ax, [di+0x1A]       ; first cluster
    mov    [bios_cluster], ax  ; save first bios cluster
    pop    ds                  ; restore ds
    
    ; ******  read FAT into buffer ****** 
    ; reuse DAP structure - setup DAP for FAT read. Read in full FAT
    ; at 0xD000
    mov    DAP.DAP_buffer_off, BufferOff
    mov    DAP.DAP_buffer_seg, BufferSeg	
    mov    ax, [BPBTable.BPB_FATSz16]
    mov    DAP.DAP_num_sectors, ax    
    mov    ax, word ptr [fat_start]
    mov    dx, word ptr [fat_start+2] 
    mov    word ptr DAP.DAP_sector_low, ax		
    mov    word ptr DAP.DAP_sector_low+2, dx 
    call   ReadDrive            ; read FAT into memory at 0xD000
    jnc	   read_fat
    
    ; Error 5
    mov    dx, offset [error5]
    call   PrintMsg             ; Error 5 - FAT read error
    jmp $

read_fat:
    ; set up ds:di to the FAT buffer
    mov    ax, BufferOff
    mov    si, ax
    mov    ax, BufferSeg
    mov    ds, ax
    mov    ax, [ds:si]
    sub    ax, 0xFFF8          ; FAT byte 0 and 1 signature
    je     good_fat_table      ; first word should be 0xFFF8 or error
    
    ; Error 6
    mov    dx, offset [error6]
    call   PrintMsg            ; Error 6 - FAT table bad
    jmp $
    
good_fat_table:
    push   dx                  ; do I need to preserve dx?
    ; Info Msg 2 - FAT table read
    mov    dx, offset [info2]  ; Good FAT read in
    call   PrintMsg
    pop    dx

; At this point, the entire FAT is loaded at 0xD000 and ds:si are set
; to that seg:off. Assume es - 0, set es:di 0000:[clusterlist]
   
    xor    ax, ax
    mov    es, ax              ; make sure es 0
    lea    di, clusterlist
    mov    ax, bios_cluster    ; cluster number from root dir entry
    
    ; read the remaining clusters from FAT loaded at 0xD000
next_clust:
    stosw                      ; store cluster number, inc next after store
    
    ; a cluster is two bytes so to get the position in the FAT
    ; cluster * 2
    ; FAT position one should be FF FF  (media byte)
    ; FAT position is partition state  FF FF - clean   FF F7 - dirty 
    add    ax, ax              ; cluster * 2
    add    ax, BufferOff       ; add the buffer offset
    mov    si, ax              ; setup for read from FAT
    mov    ax, [ds:si]         ; load the next cluster number ax
   
    ; In some docs the end-of-clusterchain marker is listed as FFF8, but
    ; in others FFFF (FAT16). From what I have seen FFFF is the correct 
    ; marker and FFF8 is only at cluster 1 position. 
    ; Unused (0x0000)
    ; Cluster in use by a file
    ; Bad cluster (0xFFF7)   <-- consider this?
    ; Last cluster in a file (0xFFF8-0xFFFF)
    cmp    ax, 0xFFF8          ; end of cluster chain >=0xFFF8
    jb     next_clust

    xor    ax, ax              ; mark end of temp cluster list 0x0000
    stosw 
    
    ; set ds back to 0x0000 so my next int 13 0x42 reads the DAP at the
    ; correct address
    mov    ds, ax  
    
    ; ************************************************************** 
    ; ****      Note: buffer at 0xD000 no longer needed         **** 
    ; **************************************************************  
    ;  Load Bios
    ; **************************************************************    

    ; Setup DAP to read in WATBIOS.BIN
    mov    DAP.DAP_buffer_off, BIOSOff   ; load to DAP off -- Offset
    mov    ax, BIOSSeg                   ; load segment
    mov    DAP.DAP_buffer_seg, ax        ; load to DAP seg -- 0x0000
    mov    al, [BPBTable.BPB_SecPerClus] ; each read will be size of cluster
    cbw
    mov    DAP.DAP_num_sectors, ax       ; load in DAP

    ; LBA_sector = ((cluster_number - 2) * BPB_SecPerClus) + data_start
    ; Start read at LBA_sector for BPB_SecPerClus
    ; then read next cluster number and repeat until loaded 

    ; This will work with this image, 4 sec per cluster * bytes per sec
    mul    BPBTable.BPB_BytsPerSec
    push   ax                      ; number of bytes each read cycle

    xor    ax, ax
    mov    es, ax 
    lea    di, clusterlist         ; setup to read stored cluster list

next_cluster:
    mov    ax, [es:di]             ; load cluster id
    test   ax, ax                  ; test ax == 0 - ZF if end cluster chain
    jz     load_finished           ; BIOS load is done - jump out 
    push   ax                      ; cluster ID - read bytes per cycle on stack already   
    
    ; load start of data  
    mov    ax, word ptr [data_start]     ; low word data_start
    mov	   word ptr DAP.DAP_sector_low, ax		
    mov    ax, word ptr [data_start+2]   ; high word data_start
    mov	   word ptr DAP.DAP_sector_low+2, ax  
    pop    ax                            ; cluster ID

    ; do the math for the correct LBA
    xor    dx, dx
    sub    ax, 0x0002                    ; subtract 2, comp for FAT position
    mul    [BPBTable.BPB_SecPerClus]     ; dx:ax
    add    word ptr [DAP.DAP_sector_low], ax
    add    word ptr [DAP.DAP_sector_low+2], dx

    call   ReadDrive
    jnc    readsetup
    
    ; Error 7
    mov    dx, offset [error7]
    call   PrintMsg              ; Error 7 - Error reading bios
    jmp $

readsetup:
    add    di, 0x02
    pop    ax                    ; number of bytes each read cycle
    push   ax
    add    ax, [DAP.DAP_buffer_off]  ; add bytes read to offset read buffer
    mov    DAP.DAP_buffer_off, ax
    jnc    next_cluster          ; not over 0xFFFF
    ;adc    [DAP_buffer_seg]     ; Future - remember for larger load
    jmp    next_cluster

load_finished:
    push   dx
    mov    dx, offset [info3]
    call   PrintMsg
    pop    dx

    ; **************************************************************  
    ;  Bios Loaded
    ;  Everything above is file system related
    ; **************************************************************  
    ;
    ;  Following calls:
    ;  1. SetA20
    ;  2. CheckCPU
    ;  3. Memory Map
    ;  4. Get System Info
    ;  5. Get Video info
    ;  6. Setup GDT
    ;  7. Setup IDT
    ;  8. Load passable info
    ;  9. Go to PMODE - transfer to watinit
     
    ; Set A20 Gate ****************************************************
    call   Seta20
    cmp    ax, 0x01
    jz     checkcpu
    
    ; Error 8
    mov    dx, offset [error8]
    call   PrintMsg              ; Error 8 - A20 not enabled
    jmp $
    
checkcpu:
    push   dx
    mov    dx, offset [info4]
    call   PrintMsg
    pop    dx
 
    ; Check for min exceptable CPU ************************************  
    call   CheckMinCPU
    cmp    ax, 0x01
    jz     exitmsg
    
    ; Error 9
    mov    dx, offset [error9]    
    call   PrintMsg              ; Error 9 - 286 cpu or less capable
    jmp $
        
exitmsg:
    mov    dx, offset [info5]
    call   PrintMsg
    mov    dx, offset [vendor_id]
    call   PrintMsg
    
    ; setup memory map ************************************************
    call   GetMemMap
    jnc    mm_done
    
    ; Error 10
    mov    dx, offset [error10]    
    call   PrintMsg              ; Error 10 - E802 function failed early
    jmp $
    
mm_done:
    mov    dx, offset [info6]
    call   PrintMsg  
    
    ; retrieve misc system info in real mode **************************
    call   GetSysInfo  

    ; retrieve video info in real mode ********************************
    call   GetVidControl
    
    ; move and setup GDT for pmode - ring 0 ***************************
    call   MoveGDT

; ********************************************************************
; At this point, BIOS is loaded at BIOSOff, A20 is enabled, and the
; cpu is valid, GDT at GDTLin
; ********************************************************************

    ; !!! leave interupts off until IDT is setup in next stage !!!
    cli                        
        
    xor    ax, ax
    mov    ds, ax
.386p
    ; setup/reserve dummy IDT
    lidt   fword ptr [idtr]
    
    mov    dx, offset [info7]
    call   PrintMsg 

    mov    dx, offset [startbios]
    call   PrintMsg 

    lgdt   fword ptr [GDTOff]  ; load gdt register
    mov    eax, cr0            ; switch to pmode by
    or     al, 1               ; Set bit PE (bit 0) of CR0
    mov    cr0, eax            ; reload cr0
    
    mov    [RMTable.IDTAddr], IDTLoc
    mov    [RMTable.GDTAddr], GDTLin
    mov    [RMTable.MEMMapAddr], MEMMapLin
    
    mov    di, ds
    mov    si, offset [RMTable]
    
    ; far jump into protected mode to clear prefetch queue and set cs
    ; why hand code? we are in 16 bit and the compiler will compile it as
    ; a 16 bit jump - hand code to use a 32 bit jmp using selector 0x08 cs
    ; default jmpf 0x08:0x10000
    db 0x66
    db 0xEA
    dd 0x10000   ; BIOSLin
    dw 0x08      ; CS Seg
; ---> in watbios and in 32 bit protected mode ring 0 here
;      "With great power comes great responsibility"

; *********************************************************************
;
;  Sub-Routines follow ----
;    - ReadDrive
;    - LoadMSG
;    - PrintMsg
;    - MoveBPB
;    - MoveGDT
;    - GetMemMap
;    - Seta20
;    - CheckMinCPU
;    - GetSysInfo
;
; *********************************************************************
; ReadDrive 
; 
; Does LBA read based on DAP information
;
ReadDrive    proc    near
    ; use LBA read not old CHS method
    mov    ah, 0x42
    mov    dl, BPBTable.BS_DrvNum
    lea	   si, DAP
    int    0x13                
    ret
ReadDrive    endp

; *********************************************************************
; LoadMSG 
; 
; Clear screen and display runnning message
;
LoadMSG    proc    near
    ; clear screen, position cursor, and print start message
    ; lifted most this from http://joebergeron.io/posts/post_two.html
    mov    ah, 0x07        ;# tells BIOS to scroll down window
    mov    al, 0x00        ;# clear entire window
    mov    bh, 0x07        ;# white on black
    mov    cx, 0x00        ;# specifies top left of screen as (0,0)
    mov    dh, 0x18        ;# 18h = 24 rows of chars
    mov    dl, 0x4f        ;# 4fh = 79 cols of chars
    int    0x10            ;# calls video interrupt
    mov    dx, 0x00        ;# get the argument from the stack. |bp| = 2, |arg| = 2
    mov    ah, 0x02        ;# set cursor position
    mov    bh, 0x00        ;# page 0 - doesn't matter, we're not using double-buffering
    int    0x10    
    mov    dx, offset [runningmsg]
    call   PrintMsg
    ret
LoadMSG    endp

; *********************************************************************
; PrintMsg 
; 
; Int 12 wasn't displaying messages, so I needed a quick Int 10.
;
; I will use this to display "$" terminated strings for informational and
; error messages. After string is displayed a LF and CR are added.
; Offset to the string is passed in dx, ds and si are saved 
; and restored on exit, ds is set to 0 for the string read. 
;  
PrintMsg    proc
    push   si
    push   ds
    mov    si, dx
    xor    dx, dx
    mov    ds, dx
continue:
    lodsb
    cmp    al, 0x24    ; look for "$" end of string
    jz     msgdone
    mov	   ah, 0x0E
    mov    bx, 0xFF
    int	   0x10  
    jmp    continue
msgdone:
    mov    ax, 0x0E0D    ; line feed and cr
    int    0x10
    mov    ax, 0x0E0A    
    int    0x10 
    pop    ds
    pop    si   
    ret
PrintMsg    endp

; *********************************************************************
; MoveBPB 
; 
; Moves loaded BPB into the new table
;
MoveBPB    proc   near
    ; overlay the BPB data area into watload.bin so we can trash
    ; the BPB area
    mov    cx, (SIZEOF BPB_FAT)
    xor    ax, ax
    mov    ds, ax
    mov    es, ax              
    mov    si, 0x7C03               ; source set ds:si to 0x7C03 in BPB
    mov    di, offset BPBTable      ; set es:di to new table 
    rep movsb
    ret
MoveBPB    endp

; *********************************************************************
; MoveGDT 
; 
; Moves GDT info to GDTSeg:GDTOff
;
GDT_Move       equ (gdt_end - gdt)

MoveGDT    proc   near
    xor    ax, ax    
    mov    ds, ax
    mov    ax, GDTSeg
    mov    es, ax
    mov    di, GDTOff    
    mov    ax, 0x90
    mov    cx, 0x100           ; Fill 256 bytes with nop 
    rep	stosb
    mov    cx, GDT_Move
    mov    si, [gdt]           ; source set ds:si to gdt:
    mov    di, GDTOff          ; set es:di to new table location
    rep movsb
    ret
MoveGDT    endp

; *********************************************************************
; GetMemMap 
;
; INT 0x15, eax= 0xE820 BIOS function to get a memory map
;
; Ref:
; https://wiki.osdev.org/Detecting_Memory_(x86)#Getting_an_E820_Memory_Map
;
; doshlp.asm:  getdhphysmem - Get configuration information 
; cdoshlp.c
;

; signature
SMAP    equ 0x0534D4150

; following clears memmap area filling with 0x90
GetMemMap    proc   near
    xor    ax, ax    
    mov    ds, ax
    mov    ax, MEMMapSeg
    mov    es, ax
    mov    di, MEMMapOff    
    mov    ax, 0x90
    mov    cx, 0x200           ; Fill 512 bytes with nop 
    rep	stosb

; inputs: es:di -> destination buffer for 20 or 24 byte entries
; outputs: bp = entry count, trashes all registers except esi
;
; MEMMapLin -- default 0x0900 the entries will be stored 
;
; Linux defines
;#define BIOS_BEGIN		0x000a0000
;#define BIOS_END		0x00100000
;
;#define HIGH_MEMORY		0x00100000
;
;#define BIOS_ROM_END		
;
;	E820_TYPE_RAM		= 1,
;	E820_TYPE_RESERVED	= 2,
;	E820_TYPE_ACPI		= 3,
;	E820_TYPE_NVS		= 4,
;	E820_TYPE_UNUSABLE	= 5,
;	E820_TYPE_PMEM		= 7,
;
; I am going is ignore the ACPI attr based on Linux note:
;    This routine deliberately does not try to account for
;    ACPI 3+ extended attributes.  This is because there are
;    BIOSes in the field which report zero for the valid bit for
;    all ranges, and we don't currently make any use of the
;    other attribute bits.  Revisit this if we see the extended
;    attribute bits deployed in a meaningful way in the future.

    xor    ax, ax
    mov    ax, MEMMapSeg
    mov    es, ax
    mov    di, MEMMapOff         ; start of MM buffer 
    
    mov    ah, 0x88
    int    0x15
    jnc    mem88                 ; no error with 88
    mov    ax, signature         ; mark error

mem88:
    mov    [es:di], ax           ; --> MEMMapOff

; e801 Typical Output: 
; AX = CX = extended memory between 1M and 16M, in K (max 3C00h = 15MB)
;
; BX = DX = extended memory above 16M, in 64K blocks
;
; There are some BIOSes that always return with AX = BX = 0. Use the CX/DX pair 
; in that case. Some other BIOSes will return CX = DX = 0. Linux initializes the ;
; CX/DX pair to 0 before the INT opcode, and then uses CX/DX, unless they are 
; still 0 (when it will use AX/BX)
;
; With that in mind, this is the Linux code that checks:
;  	/* Do we really need to do this? */
;	if (oreg.cx || oreg.dx) {
;		oreg.ax = oreg.cx;
;		oreg.bx = oreg.dx;
; 
; We will just stuff all four the values and sort it out later
;
e801:
    add    di, 0x02 
    xor    cx, cx
    xor    dx, dx
    mov    ax, 0xE801
    int    0x15
    jnc    e801a                 ; no error with 801
    mov    ax, signature         ; mark error
    mov    bx, ax    
    mov    cx, ax  
    mov    dx, ax     
    
e801a:
    mov    [es:di], ax           ; --> MEMMapOff + 2
    add    di, 0x02
    mov    [es:di], bx           ; --> MEMMapOff + 4
    add    di, 0x02
    mov    [es:di], cx           ; --> MEMMapOff + 6
    add    di, 0x02
    mov    [es:di], dx           ; --> MEMMapOff + 8

e820:
    add    di, 0x08              ; first e820 entry at  MEMMapOff + 0x10    
    xor    ebx, ebx	             ; "continuation value" -- ebx must be 0 to start
    xor    bp, bp                ; keep an entry count in bp
    mov    edx, SMAP             ; Place "SMAP" into edx
    mov    eax, 0xE820           ; function code
    mov    ecx, 0x18	         ; ask for 24 bytes but will only use 20
    int    0x15
    jc     failed                ; carry set on first call means "unsupported function"
    mov    edx, SMAP             ; Some BIOSes apparently trash this register?
    cmp    eax, edx	             ; on success, eax must have been reset to "SMAP"
    jne    failed
    test   ebx, ebx	             ; ebx = 0 implies list is only 1 entry long (worthless)
    je     failed
    add    di, 0x18              ; increment to next entry

next_mem_entry:
    ; Note: I am going to keep 0 length entries for now
	; es:di will point to next entry
	; bx contains "continuation value"
	
    mov    eax, 0xe820           ; eax, ecx get trashed on every int 0x15 call
    mov    ecx, 0x18             ; ask for 24 bytes again
    int    0x15
    jc     mem_list_end          ; carry set means "end of list already reached"
    mov    edx, SMAP             ; Some BIOSes apparently trash this register?
    cmp    eax, edx	             ; on success, eax must have been reset to "SMAP"
    jne    failed

    add    di, 0x18              ; increment to next entry
    inc    bp
    test   ebx, ebx              ; if ebx resets to 0, list is complete
    jne    next_mem_entry

mem_list_end:
    inc    bp                    ; had a normal carry set/end - inc count
    ; save bp count of entries - add sig on either side just for debug
    mov    ax, (MEMMapOff + 0x0A)  ;--> MEMMapOff + 10 *** this is where e820 count will live
    mov    di, ax
    
    mov    ax, bp
    cbw
    mov    [es:di], ax      

; added int 12  13Oct19
    clc
    int    0x12                 ; just get int12 to verify bottom of EBDA
    jc     failed
    add    di, 0x02
    mov    [es:di], ax
    
    add    di, 0x02 
    mov    ax, signature
    mov    [es:di], ax
    clc                         ; clear carry before return 
    ret

failed:
    stc
    ret
    
GetMemMap    endp

; *********************************************************************
; Seta20 
; 
; Check and enable A20 gate to access high memory.
;
; Entry: Nothing saved - could trash registers
; Return: ax 0x01 if A20 enabled and 0x00 if not - you handle the error
;         dx has debug info
;
; Dev being done using bochs and A20 is emabled by default
;
; I just try int 0x15, its 2019 - get a newer system
;
Seta20    proc
    mov    ax, 0x2402
    int    0x15
    cmp    ax, 0x01            ; A20 already enabled
    jz     donea20
    mov    ax, 0x2403
    int    0x15                ; int 0x15 supported? Damn better be in 2019
    jc     a20error
    mov    ax, 0x2401
    int    0x15                ; just enable
    jc     a20error    
    mov    ax, 0x2402
    int    0x15                ; check enabled
    jc     a20error     
    jmp    donea20             ; ax already 0x01 if enabled on int 0x15 return
a20error:
    push   ax  
    xor    dx, dx
    pop    dx                  ; return a status for debug
    xor    ax, ax              ; error       
donea20:
    ret
Seta20   endp

; *********************************************************************
; CheckCPU 
; 
; Just to be complete, going to check for greater than 286 before going to
; the BIOS file, just in case running on a misconfigured emulator. Do 8086 or
; 286s still exist? LOL
;
; Entry: Nothing saved - could trash registers
; Return: ax 0x01 if good cpu and 0x00 if not - you handle the error
; This is old code I used years ago.
;    
CheckMinCPU    proc
    pushf                        ; push original FLAGS
    pop    ax                    ; get original FLAGS
    mov    cx, ax                ; save original FLAGS
    and    ax, 0fffh             ; clear bits 12-15 in FLAGS
    push   ax                    ; save new FLAGS value on stack
    popf                         ; replace current FLAGS value
    pushf                        ; get new FLAGS
    pop    ax                    ; store new FLAGS in AX
    and    ax, 0f000h            ; if bits 12-15 are set, then
    cmp    ax, 0f000h            ; processor is an 8086/8088
    jne    check_80286           ; go check for 80286
    xor    ax, ax
    jmp    end_cpu_type

; Intel 286 processor check
; Bits 12-15 of the FLAGS register are always clear on the
; Intel 286 processor in real-address mode.
.286
check_80286:
    smsw   ax                    ; save machine status word
    and    ax, 1                 ; isolate PE bit of MSW
    or     cx, 0f000h            ; try to set bits 12-15
    push   cx                    ; save new FLAGS value on stack
    popf                         ; replace current FLAGS value
    pushf                        ; get new FLAGS
    pop    ax                    ; store new FLAGS in AX
    and    ax, 0f000h            ; if bits 12-15 are clear
    jnz    check_80386           ; jump if processor is 80286
    xor    ax, ax
    jmp    end_cpu_type

; Intel386 processor check
; The AC bit, bit #18, is a new bit introduced in the EFLAGS
; register on the Intel486 processor to generate alignment
; faults.
; This bit cannot be set on the Intel386 processor.
.386   
check_80386:
    pushfd                       ; push original EFLAGS
    pop    eax                   ; get original EFLAGS
    mov    ecx, eax              ; save original EFLAGS
    xor    eax, 40000h           ; flip AC bit in EFLAGS
    push   eax                   ; save new EFLAGS value on stack
    popfd                        ; replace current EFLAGS value
    pushfd                       ; get new EFLAGS
    pop    eax                   ; store new EFLAGS in EAX
    xor    eax, ecx              ; can't toggle AC bit, processor=80386
    jnz    check_80486           ; jump if not 80386 processor
    xor    eax, eax
    jmp    end_cpu_type
check_80486:
    push   ecx
    popfd                        ; restore AC bit in EFLAGS first

; Intel486 processor check
; Checking for ability to set/clear ID flag (Bit 21) in EFLAGS
; which indicates the presence of a processor with the CPUID
; instruction.
.486
    mov    eax, ecx              ; get original EFLAGS
    xor    eax, 200000h          ; flip ID bit in EFLAGS
    push   eax                   ; save new EFLAGS value on stack
    popfd                        ; replace current EFLAGS value
    pushfd                       ; get new EFLAGS
    pop    eax                   ; store new EFLAGS in EAX
    xor    eax, ecx              ; can't toggle ID bit,
    jne    check_80586           ; processor=80486
    xor    eax, eax
    jmp    end_cpu_type
check_80586:
.586
    mov    eax, 0x01             ; get family/model/stepping/features
    cpuid
    mov    [version], eax
    mov    [features], edx
    shr    eax, 8                ; isolate family
    and    eax, 0fh
    cmp    eax, 0x06
    jge    cpuisgood
    xor    eax, eax
    jmp    end_cpu_type  
cpuisgood:
    mov    eax, 0x00
    cpuid
    mov    dword ptr [vendor_id], ebx
    mov    dword ptr [vendor_id+4], edx
    mov    dword ptr [vendor_id+8], ecx
.8086
    xor    ax, ax
    inc    ax                    ; good to continue to bios
end_cpu_type:
    ret
CheckMinCPU    endp

; *********************************************************************
; GetSysInfo 
; 
; Call INT 11, 1A, 15-C0 to get system info before heading to
; protected mode.
;
; Entry: Nothing saved - could trash registers
; Return: 
;   bits 15-14 = no. of printers
;   NA      13 game adapter installed
;	NA      12 unused, internal modem (PS/2)
;   bits  11-9 = no. of serial ports
;   bits   7-6 = no. of diskettes
;   bits   5-4 = primary display
;   NA     2-3 old PC system board RAM < 256K
;   bit      1 = math coprocessor
;   bit      0 = IPL diskette installed
;
; called after cpu check
.686
GetSysInfo    proc
    xor    ax, ax
    int    0x11
    mov    [RMTable.INT11RET], ax

    fninit
    fninit

    xor    edi, edi        
    mov    ax, 0xB101            ; http://www.ctyme.com/intr/rb-2371.htm
    int    0x1A
    cmp    edx,020494350h        ; ICP
    
    ; error handle -- save info edi 32bit entry

; Get INT15 C0 data and store in real mode data table
    
    mov    ah, 0xC0              ; http://www.ctyme.com/intr/rb-1594.htm
    int    0x15                  ; http://www.oldlinux.org/Linux.old/docs/interrupts/int-html/rb-1594.htm
    jc     si_error
    and    ah, ah
    jne    si_error              ; get C0 INT error if bad and halt - any error 
                                 ; means I need to fix something
    mov    cx, [es:bx] 
    mov    [RMTable.INT15C0Size], cx

    mov    si, offset RMTable.INT15C0Data
    
    inc    bx
    inc    bx                    ; move to first feature byte

INT15COLoop:    
    mov    al, byte ptr [es:bx]
    mov    byte ptr [ds:si], al
    inc    bx
    inc    si
    dec    cx
    cmp    cx, 0x00
    jne    INT15COLoop

    ret

si_error:
    mov    dx, offset [error11]
    call   PrintMsg            ; Error 11 - BIOS-INT not supported or error
    jmp $

GetSysInfo    endp

; *********************************************************************
; GetVidControl 
; 
; Call INT 10 to get SuperVGA information
;
; Returns:
;
;    .signature db "VESA";     must be "VESA" to indicate valid VBE support
;    .version resw 1;          VBE version; high byte is major version, low byte is minor version
;    .oem resd 1;              segment:offset pointer to OEM
;    .capabilities resd 1;     bitfield that describes card capabilities
;    .video_modes resd 1;      segment:offset pointer to list of supported video modes
;    .video_memory resw 1;     amount of video memory in 64KB blocks
;    .software_rev resw 1;     software revision
;    .vendor resd 1;           segment:offset to card vendor string
;    .product_name resd 1;     segment:offset to card model name
;    .product_rev resd 1;      segment:offset pointer to product revision
;    .reserved resb 222;       reserved for future expansion
;    .oem_data resb 256;       OEM BIOSes store their strings in this area

GetVidControl    proc
    mov    ax, 0x4F00
    mov    di, offset vesabuff
    push   cs
    pop    es
    mov    byte ptr [es:di]  , 'V'
    mov    byte ptr [es:di]+1, 'B'
    mov    byte ptr [es:di]+2, 'E'
    mov    byte ptr [es:di]+3, '2'
    int    0x10

    cmp    al, 0x4F                  ; first check                  
    jnz    novesa

    mov    cx, 0x04                  ; second check - VESA string
    mov    si, offset vidsig
    mov    di, offset vesabuff
    repz cmpsb
    jnz    novesa
    
    mov    di, offset vesabuff       ; This is a WTF the version is supposed 
    mov    al, byte ptr [es:di]+4    ; be byte - byte, but it reads in other 
    mov    RMTable.VESAMinor, al     ; places like word (l-h)  
    mov    al, byte ptr [es:di]+5
    mov    RMTable.VESAMajor, al   
    mov    ax, word ptr [es:di]+6    ; reading word now!
    mov    RMTable.VESATotalMem, ax   

    ; get pmode interface
    mov    ax, 0x4F0A
    mov    di, offset vesabuff
    push   cs
    pop    es
    mov    byte ptr [es:di]  , 'V'
    mov    byte ptr [es:di]+1, 'B'
    mov    byte ptr [es:di]+2, 'E'
    mov    byte ptr [es:di]+3, '2'
    int    0x10

    cmp    al, 0x4F                  ; first check                  
;    jnz    novesa

    mov    cx, 0x04                  ; second check - VESA string
    mov    si, offset vidsig
    mov    di, offset vesabuff
    repz cmpsb
;    jnz    novesa


    ret

novesa:
    mov    dx, offset [error12]
    call   PrintMsg            ; Error 11 - BIOS-INT not supported or error
    jmp $

GetVidControl    endp

;**************************************************************************
;
;  iodelay.asm
;
;  Description: Calculate io delay based on number of loops
;
;  Very old, from an old loader project (circa 2008ish)- try to add  
;  in case I need it -- MKG November 2019
;==========================================================================
;
;  - Times number of loops (105588) using PIT counter 0.
;
;  - Timer operations: 1. set to start (T0)
;                      2. get start time (T1)
;                      3. run loops
;                      4. get end time (T2)
;
;  - Get interval between start and end time, correct based
;    on set to start -- (T2 - T1) - T0 (refcount)
;
;  - Each counter tick is 839ns so 105588 (loops)/839 results in
;    number of loops per tick
;
;  Note: After a lot of head scrtatching and running numbers through
;  a spread sheet, I found that the result was approx 1.678 ratio to
;  the above. So, it seems the result is corrected to approx 500ns
;  839/1.678. I have no idea how acurate this value must be, but
;  remainders were rounded up therefore I assume the 500ns is a
;  minimum.
;
;  - Given the above 105588 (loops) * 500 / 839 gets the iodelay
;
;  Might needs some more work, but is working on my systems. Keep storing
;  T1 and T2 values for bug check.
;
;**************************************************************************

; TmpIODelay
;
; After reading a bit I know this is a delay to let the PIT
; settle between reads and writes
;
; I just kept this as I saw via Bochs, however, I did see a
; number of examples that used "jmp  $+2". Freeldr used a
; loop of 256.
;
; This might have to be altered based on system speed??
;
; 3 Jul 08 - running on a real system seems to result in an almost
; constant result. However, running on Virtual PC results in large
; differences.

TmpIODelay  proc near

    push    eax
    mov     ax, TMPDELAYNUM  ; see note above

_tmpio_delay_loop:
    dec     ax
    jnz    _tmpio_delay_loop

    pop    eax
    ret

TmpIODelay  endp


; FindIODelay
;
; Used Bochs output and periphs.asm (freeldr) to figure the following.
; FREELDR Ref: NB: The routines _FindIODelay and _IODelay are taken from
; Frank van Gilluwe, *The Undocumented PC*, 2d. ed., pp 42-44.

FindIODelay  proc near

    ; 11/5/19 - do not need to save anything
    ; save registers and segs
    ;pusha
    ;push   ds
    ;push   cs
    ;pop    ds
    ;pushf               ; save flags
    ;cli                 ; stop interupts


    ; *** Setup counter 0 -- T0
    xor    ebx, ebx       ; zero bx, will be count of 65536

    ; This routine sets counter 0 of the 8254 chip to interrupt
    ; in mode 2 18.2 times per second -- from freeldr comment

    ; setup PIT 0  0x34  00110100b
    ; Bits 7-6: 00 : Select Counter (0-2)
    ; Bits 5-4: 11 : Command: (11) means R/W 7-0, then 15-8
    ; Bits 3-1: 010 : Mode: (010) means rate generator (divide by n)
    ; Bit 0: 0: Binary
    mov    al, SC_CNT0+RW_LSBMSB+CM_MODE2+CW_BIN
    out    PORT_CW, al
    call   TmpIODelay

    ; In the next stanza we write 0 twice to port 40h.  This has the effect
    ; of writing the 16-bit word 0, which the PIT chip interprets as the
    ; value 64k.  Since the clock tick is 1.193180 MHz, the PIT chip will
    ; then cause an IRQ0 about every 56ms, or about 18.2 times per second.
    ;
    ; MKG:
    ; 8254 base clock frequency of 1193180Hz
    ; 1/f = T so 838ns @ tick
    ; counter 2^16 = 65536 so max count 55ms
    ;
    ; Simple walkthrough:
    ; - First step is to setup and start counter 0 - while this is a start point
    ;   I'll call it Ref (bx) [above]
    ; - Next setup and start counter with result Start in cx
    ; - Run a fixed loop
    ; - Get the counter0 ticks with result End in dx

    mov    ax, bx
    out    PORT_CNT0, al
    call   TmpIODelay
    mov    al, ah
    out    PORT_CNT0, al
    call   TmpIODelay


    ; *** timer setup and read to cx -- T1 ***
    ; setup PIT 0  0x00
    mov    al, SC_CNT0+RW_LATCHCNT+CM_MODE0+CW_BIN
    out    PORT_CW, al
    call   TmpIODelay
    in     al, PORT_CNT0        ; read LSB into cl and MSB into ch
    call   TmpIODelay
    mov    cl, al
    in     al, PORT_CNT0
    mov    ch, al


    ; *** delay loop ***
    ; runs 105588 loops while couter 0 counts
    mov    esi, 0x19C74   ; 105588
_delayloop:
    dec    esi
    jnz    _delayloop


    ; *** timer setup and read to dx -- T2 ***
    ; setup PIT 0
    mov    al, SC_CNT0+RW_LATCHCNT+CM_MODE0+CW_BIN
    out    PORT_CW, al
    call   TmpIODelay
    in     al, PORT_CNT0        ; read LSB into dl and MSB into dh
    call   TmpIODelay
    mov    dl, al
    in     al, PORT_CNT0
    mov    dh, al

    popf                ; restore flags

    ; *** calculate io_count ***
    ; remeber counter couts down so results are negative

    ; T0 -- bx -- zero
    ; T1 -- cx
    ; T2 -- dx
    mov    RMTable.count_t1, cx
    mov    RMTable.count_t2, dx

    ; get time values
    sub    bx, cx       ; (0 - (-cx) = bx
    sub    cx, dx       ; ((-cx) - (-dx)) = cx

    ; bx contains diff between T0 ref and T1 start
    ; cx contains total time interval between T1 and T2

    ; check for a diffrence between T0 and T1, the result will
    ; be the value expected for timer setup and overhead

    and    bx, bx       ; diff between T0 and T1 zero ? But I would guess
                        ; the diff between T0 abd T1 should never be zero
    jz     _chkinterval ; yes - jmp - damn fast system or something wrong

    sub    cx, bx       ; interval compensate for setup ticks
                        ; (TmpIODelay, etc... ?) (cx - bx) = cx
_chkinterval:
    ; a zero here will get us a DIV by Zero
    ; but really, if we get T1 to T2 zero then something
    ; has got to be wrong
    or     cx, cx
    jz     _iocnterr

    ; have to handle what ifs - T2 is equal to or below T1
    ; how's this going to happen unless it wraps maybe?
    ; who cares -- just max out and see what happens.
    cmp    cx, bx
    jbe   _iocnterr

    ; 0x19C74 (105588) loops / corrected count (bx)
    mov    dx, 1
    mov    ax, 0x9C74
    div    cx
    or     dx, dx       ; remainder? round up
    jz     _no_remainder1
    inc    ax

_no_remainder1:

    ; ((result above) * 500) / 839
    mov    bx, 0x1F4
    mul    bx
    mov    bx, 0x347
    div    bx
    or     dx, dx       ; remainder? round up
    jz     _io_set
    inc    ax
    jmp    _io_set

_iocnterr:
    ; something is wrong with the timer so just max-out
    ; the delay - 0xFFFF (65535)
    mov    ax, 0xFFFF
    jmp    _io_set

_io_set:
    mov    [RMTable.io_count], ax

    ; restore registers and segs
    pop    ds
    popa
    ret

FindIODelay  endp

; ********************************************************************
; * End of watload code
; ********************************************************************

; ********************************************************************
; ****************** Keep in sync with BPB ***************************
; We do a direct copy from the BPB to here from absolute to location 
; offset of new BPBTable
BPBTable BPB_FAT < >

; Real mode data to pass to WatBIOS
RMTable realmode_data < > 

; DAP : Disk Address Packet
DAP DAP_TABLE <0x10,0x0,0x0,0x0,0x0,0x0,0x0> 

; fat_start and data_start passed from bootsector
fat_start       dd  0x00        ; first FAT sector - passed in cx/dx
data_start      dd  0x00        ; first Data sector - passed in si/di

;sys_info        dw  0x00        ; returned from INT 0x11

bios_cluster    dw  0x00        ; bios cluster from root dir
bios_size       dd  0x00        ; bios file size in bytes

; CPU info table
vendor_id       dd    0,0,0	
vendor_id_term  db    '$' 
version	        dd    0
features        dd    0

; ************* GDT data for pmode entry ***********************
; location of the GDT description structure for LGDT instruction
; Intel Vol 3a p3-15
;
; GDT 64 bits (8 bytes)
; 1st Double word:
;Bits   Function         Description
;0-15   Limit 0:15       First 16 bits in the segment limiter
;16-31  Base 0:15        First 16 bits in the base address
;
;2nd Double word:
;Bits	Function         Description
;0-7    Base 16:23       Bits 16-23 in the base address
;8-12   Type Segment     type and attributes
;13-14  Privilege Level  0 = Highest privilege (OS), 3 = Lowest privilege (User applications)
;15     Present flag     Set to 1 if segment is present
;16-19  Limit 16:19      Bits 16-19 in the segment limiter
;20-22  Attributes       Different attributes, depending on the segment type
;23     Granularity      Used together with the limiter, to determine the size of the segment
;24-31  Base 24:31       The last 24-31 bits in the base address
;
; * Access Byte
; Present      1        
; Privl        00 Ring 0
; Type         1
; Executable   1 for code   0 for data
; Direction    Data 0/1 segment grows
;              Code 0/1 execute ring 
; R/W          1/0
; Accessed     0 initial
;
; Code 10011010 = 9A  Data 10010010 = 92
;
; * Flags
; Granularity  1 in blocks (pages) of 4 KB
; Size         1 32 bit pmode -- limit 4G
;
; Code 1100   Date 1100 
; Flags + 16-19 limit = 1100 1111 = 0xCF
;
GDT_Size       equ (gdt_end - gdt - 1)

Code_Desc      equ  0x08
Data_Desc      equ  0x10

; Dummy IDT for pmode jump
idtr:
    dw    IDTSize
    dd    IDTLoc

; GDT description structure
gdt:
    ; Null descriptor
    ; first descriptor in the GDT is not used by the processor, so
    ; since null descriptor not used, store GDT size and location in it  
    dw    GDT_Size
    dw    GDTOff
    dw    GDTSeg
    dw    0x0000
    ; 32 Code descriptor
    db    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x9A, 0xCF, 0x00
    ; 32 Data descriptor
    db    0xFF, 0xFF, 0x00, 0x00, 0x00, 0x92, 0xCF, 0x00
; add -->
    ; 16 Code descriptor 
    ; 16 Data descriptor
    ; Userspace code/data segments
    ; TSS for each cpu????
gdt_end:

; GDT info after load and switch
; Global Descriptor Table (base=0x0000000000000500, limit=23):
; GDT[0x0008]=Code segment, base=0x00000000, limit=0xffffffff, Execute/Read, Non-Conforming, 32-bit
; Global Descriptor Table (base=0x0000000000000500, limit=23):
; GDT[0x0010]=Data segment, base=0x00000000, limit=0xffffffff, Read/Write

; ***************** END GDT data for pmode  ****************************
; Video INT buffer
vidsig        db    'VESA'

runningmsg    db    'watload running ....$'
startbios     db    'jumping to pmode and watbios start ...$'
filename      db    "WATBIOS BIN"

; error messages sync'd with BPB messages numbers 1-6
;error1        db    '1 - No bios extensions$'  here for ref -checked in BPB
error2        db    '2 - Root directory read$'
error3        db    '3 - watbios.bin not found$'
;error4        db    '4 - Removed for more RAM space$' in BPB
error5        db    '5 - FAT read error$'
error6        db    '6 - FAT table bad$'
; watload errors only
error7        db    '7 - Error reading bios$'
error8        db    '8 - A20 not enabled$'
error9        db    '9 - 286 cpu or less capable$'
error10       db    '10 - INT15 E802 function failed$'
error11       db    '11 - BIOS-INT not supported or error$'
error12       db    '12 - VESA not supported or error$'

info1         db    '- Root directory read - file found$'
info2         db    '- FAT table read$'
info3         db    '- BIOS loaded$'
info4         db    '- A20 enabled$'
info5         db    '- Better than 686 cpu checked$'
info6         db    '- Memory map E820 stored$'
info7         db    '- GDT setup - IDT dummy set$'

; Making the assumption file size will remain small
; Tmp location of cluster chain read from dir entry and FAT
clusterlist   dw    0x32 dup(0)   ; 50 words for clusterlist

vesabuff      db    0x200 dup(0)

; so I can check end of valid load
signature     dw    0xAA55

_TEXT ends

end loader_start
