; timer.inc
;
; Pulled for use again --- Nov 2019
;
; Pulled from OS/2 DDK and format cleaned up for readability
; M Greene 25 July 2008

;***    Physical Timer Device (TIMER)
;*
;*      SCCSID = @(#)timer.h    6.2 91/02/18
;*
;*      Timer hardware constants declaration.  This includes port
;*      addresses, bit fields and interrupt constants.
;*
;*      Copyright (c) 1988,1989 Microsoft Corporation
;*
;*      MODIFICATION HISTORY
;*          12/19/88  MTS    Created.
;


; http://heim.ifi.uio.no/~stanisls/helppc/8253.html
;
;	Port 40h, 8253 Counter 0 Time of Day Clock (normally mode 3)
;	Port 41h, 8253 Counter 1 RAM Refresh Counter (normally mode 2)
;	Port 42h, 8253 Counter 2 Cassette and Speaker Functions
;	Port 43h, 8253 Mode Control Register, data format:
;
;
;	|7|6|5|4|3|2|1|0|  Mode Control Register
;	 | | | | | | | `---- 0=16 binary counter, 1=4 decade BCD counter
;	 | | | | `--------- counter mode bits
;	 | | `------------ read/write/latch format bits
;	 `--------------- counter select bits (also 8254 read back command)


;  Ports (for both PC/AT and PS/2)
PORT_CNT0               equ     0x40            ; counter 0 data port
PORT_CNT1               equ     0x41            ; counter 1 data port
PORT_CNT2               equ     0x42            ; counter 2 data port
PORT_CW                 equ     0x43            ; control word for all counters
PORT_SYSB               equ     0x61            ; system control port B

;  Values that can be written to PORT_CW
CW_BIN                  equ     0x00            ; bit clear => binary
CW_BCD                  equ     0x01            ; bit set => BCD counting
CW_CNTMODE              equ     0x0E            ; count mode 0 to 5
CW_RWMODE               equ     0x30            ; Read/Write modes
CW_SELCNT               equ     0x0C0           ; select counter

;  Values that can be written to the CW_CNTMODE field of PORT_CW
CM_MODE0                equ     0x00            ; Mode 0: Interrupt on terminal count
CM_MODE1                equ     0x02            ; Mode 1: Hardware retriggerable one-shot
CM_MODE2                equ     0x04            ; Mode 2: Rate generator
CM_MODE3                equ     0x06            ; Mode 3: Square wave mode
CM_MODE4                equ     0x08            ; Mode 4: S/W triggered strobe
CM_MODE5                equ     0x0A            ; Mode 5: H/W triggered strobe (retriggerable)
CM_CNT0                 equ     0x02            ; Counter 0 in Read-Back command
CM_CNT1                 equ     0x04            ; Counter 1 in Read-Back command
CM_CNT2                 equ     0x08            ; Counter 2 in Read-Back command

;  Values that can be written to the CW_RWMODE field of PORT_CW
RW_LATCHCNT             equ     0x00            ; Latch counter command
RW_LSB                  equ     0x10            ; Read/Write least significant byte only
RW_MSB                  equ     0x20            ; Read/Write most significant byte only
RW_LSBMSB               equ     0x30            ; Read/Write LSB first, then MSB
RW_NOLATCHCOUNT         equ     0x20            ; No latch count in Read-Back command
RW_NOLATCHSTATUS        equ     0x10            ; No latch status in Read-Back command

;  Values that can be written to the CW_SELCNT field of PORT_CW
SC_CNT0                 equ     0x00            ; Counter 0
SC_CNT1                 equ     0x40            ; Counter 1
SC_CNT2                 equ     0x80            ; Counter 2
SC_READBACK             equ     0x0C0           ; Read-Back command

;  Read-Back command format (write to PORT_CW)
RB_SELCNT0              equ     0x02            ; select counter 0
RB_SELCNT1              equ     0x04            ; select counter 1
RB_SELCNT2              equ     0x08            ; select counter 2
RB_NOLATCHSTATUS        equ     0x10            ; bit clear => latch status of counters
RB_NOLATCHCNT           equ     0x20            ; bit clear => latch count of counters

;  Status Byte format (read from corresponding counter data port)
STATUS_BCD              equ     CW_BCD          ; counter is in BCD counting mode
STATUS_CNTMODE          equ     CW_CNTMODE      ; counter mode (see CM_MODEx definitions)
STATUS_RWMODE           equ     CW_RWMODE       ; Read/Write mode (see RW_* definitions)
STATUS_NULLCNT          equ     0x40            ; Null count (new count has not been loaded)
STATUS_OUTPUT           equ     0x80            ; state of output pin of counter

;  System Control Port B Format
SYSB_RESETIRQ0          equ     0x80            ; set this bit to reset IRQ0 (W)
SYSB_PARITYCHK          equ     0x80            ; parity check has occurred (R)
SYSB_CHNCHK             equ     0x40            ; channel check has occurred (R)
SYSB_SPKOUT             equ     0x20            ; speaker timer output state (R)
SYSB_REFREQ             equ     0x10            ; refresh request toggle (R)
SYSB_PARITYENABLE       equ     0x08            ; parity check enable (R/W)
SYSB_CHNCHKENABLE       equ     0x04            ; channel check enable (R/W)
SYSB_SPKENABLE          equ     0x02            ; speaker data enable (R/W)
SYSB_SPKGATE            equ     0x01            ; speaker timer gate enable (R/W)
SYSB_SPKBITS            equ     0x03            ; speaker bit mask

;  Miscellaneous constants
TIMERIRQ                equ     0x00            ; IRQ number of timer interrupt
;TIMERFREQ               equ     1193167         ; timer frequency in Hz
; MKG: should be this?
TIMERFREQ               equ     1193180         ; timer frequency in Hz
