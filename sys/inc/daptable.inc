;***********************************************************************
; *
; *  daptable.inc -- Disk Address Packet
; *
; *  ===================================================================
; *
; *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
; *                      March 2019
; *
; *  ===================================================================
; *
; *  Description: DAP : LBA Disk Address Packet
; *
; *  ===================================================================
; *
; *   This program is free software; you can redistribute it and/or modify
; *   it under the terms of the GNU General Public License as published by
; *   the Free Software Foundation; either version 2 of the License, or
; *   (at your option) any later version.
; *
; *   This program is distributed in the hope that it will be useful,
; *   but WITHOUT ANY WARRANTY; without even the implied warranty of
; *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; *   GNU General Public License for more details.
; *
; *   You should have received a copy of the GNU General Public License
; *   along with this program; if not, write to the Free Software
; *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
; *
; **********************************************************************


DAP_TABLE    struc

DAP_size        db  0x10        ; packet size
DAP_unused      db  0x00        ; unused
DAP_num_sectors	dw  0x0000      ; number of sectors, I usually set to cluster size
DAP_buffer_off	dw  0x7E00      ; read to buffer offset
DAP_buffer_seg	dw  0x0000      ; read to buffer segment
DAP_sector_low	dd  0x00000000	; lower LBA	
DAP_sector_high	dd  0x00000000	; upper LBA

DAP_TABLE    ends
