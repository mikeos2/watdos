;***********************************************************************
; *
; *  rmdata.inc -- Real Mode data structure
; *
; *  ===================================================================
; *
; *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
; *                      November 2019
; *
; *  ===================================================================
; *
; *  Description: Structure to pass real mode data to WatBIOS
; *
; *  ===================================================================
; *
; *   This program is free software; you can redistribute it and/or modify
; *   it under the terms of the GNU General Public License as published by
; *   the Free Software Foundation; either version 2 of the License, or
; *   (at your option) any later version.
; *
; *   This program is distributed in the hope that it will be useful,
; *   but WITHOUT ANY WARRANTY; without even the implied warranty of
; *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; *   GNU General Public License for more details.
; *
; *   You should have received a copy of the GNU General Public License
; *   along with this program; if not, write to the Free Software
; *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
; *
; **********************************************************************


realmode_data    struc
IDTAddr          dw  ?     ; Dummy Interrupt Descriptor Table (IDT) location
GDTAddr          dw  ?     ; 
MEMMapAddr       dw  ?     ; Memory map location - default 0x0900
INT11RET         dw  ?     ; 
INT15C0Size      dw  ?
INT15C0Data      db  0x0F dup(?)
io_count         dw  ?
count_t1         dw  ?
count_t2         dw  ?
VESAMajor        db  ?
VESAMinor        db  ?
VESATotalMem     dw  ?
realmode_data    ends

