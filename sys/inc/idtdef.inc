;***********************************************************************
; *
; *  idtdef.inc -- loader IDT definitions
; *
; *  ===================================================================
; *
; *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
; *                      July 2019
; *
; *  ===================================================================
; *
; *  Description: List of IDT interupts
; *
; *  ===================================================================
; *
; *   This program is free software; you can redistribute it and/or modify
; *   it under the terms of the GNU General Public License as published by
; *   the Free Software Foundation; either version 2 of the License, or
; *   (at your option) any later version.
; *
; *   This program is distributed in the hope that it will be useful,
; *   but WITHOUT ANY WARRANTY; without even the implied warranty of
; *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; *   GNU General Public License for more details.
; *
; *   You should have received a copy of the GNU General Public License
; *   along with this program; if not, write to the Free Software
; *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
; *
; **********************************************************************
; Ref: http://www.independent-software.com/operating-system-development-setting-up-interrupt-descriptor-table.html


; Descriptor
; Offset  Size     Name            Description
; 0       2 bytes  Offset (low)    Low 2 bytes of interrupt handler offset
; 2       2 bytes  Selector        A code segment selector in the GDT
; 4       1 byte   Zero            Zero - unused
; 5       1 byte   Type/attribute  Type and attributes of the descriptor
; 6       2 bytes  Offset (high)   High 2 bytes of interrupt handler offset

; ---- Type/attribute
; Bits    Code                        Description
; 0..3    Gate type                   IDT gate type (see below)
; 4       Storage segment             Must be set to 0 for interrupt gates.
; 5..6    Descriptor privilege level  Gate call protection. This defines the minimum privilege level 
;                                     the calling code must have. This way, user code may not be able 
;                                     to call some interrupts.
; 7       Present                     For unused interrupts, this is set to 0. Normally, it’s 1.

; -------- Gate Types
; Value    Description
; 0x5      80386 32-bit Task gate
; 0xe      80386 32-bit Interrupt gate
; 0xf      80386 32-bit Trap gate

; interrupt gates and trap gates: 
; The difference is that when an interrupt gate is called, the CPU automatically 
; disables interrupts and enables then upon returning from the interrupt handler, 
; which it doesn’t do for trap gates.

irq0:
        dw isr0
        dw 0x0008
        db 0x00
        db 10101110b
        dw 0x0000
irq1:
        dw isr1
        dw 0x0008
        db 0x00
        db 10101110b
        dw 0x0000
irq2:
        dw isr2
        dw 0x0008
        db 0x00
        db 10101110b
        dw 0x0000
 
; ...
; ... (the code between here is not different)
; ...
 
irq32:
        dw isr32
        dw 0x0008
        db 0x00
        db 10101110b
        dw 0x0000
irq33:
        dw isr33
        dw 0x0008
        db 0x00
        db 10101110b
        dw 0x0000
