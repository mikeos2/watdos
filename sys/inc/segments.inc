; Modified for use with Open Watcom - M Greene 25 June 2008
; 15 April 2019 modified

; segments.inc - watbios segments
;
; BIOSinitEntry HAS to be the first segment!!! It contains the startup
; assembly on entry to watbios and jump to main, start of C code
;

.686

BIOSinitEntry      segment dword public use32 'START'
BIOSinitEntry      ends

_TEXT              segment dword public use32 'CODE'
_TEXT              ends

_DATA              segment dword public use32 'DATA'
_DATA              ends

CONST              segment dword public use32 'DATA'
CONST              ends

CONST2             segment dword public use32 'DATA'
CONST2             ends

_BSS               segment dword public use32 'BSS'
_BSS               ends

STACK              segment para stack use32 'STACK'
STACK              ends

DGROUP group BIOSinitEntry,_TEXT,CONST,CONST2,_DATA,_BSS,STACK

