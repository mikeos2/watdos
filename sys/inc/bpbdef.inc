;***********************************************************************
; *
; *  bpbdef.inc -- Development BPB
; *
; *  ===================================================================
; *
; *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
; *                      March 2019
; *
; *  ===================================================================
; *
; *  Description: Structure used for FAT16 drive with extension
; *
; *  ===================================================================
; *
; *   This program is free software; you can redistribute it and/or modify
; *   it under the terms of the GNU General Public License as published by
; *   the Free Software Foundation; either version 2 of the License, or
; *   (at your option) any later version.
; *
; *   This program is distributed in the hope that it will be useful,
; *   but WITHOUT ANY WARRANTY; without even the implied warranty of
; *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; *   GNU General Public License for more details.
; *
; *   You should have received a copy of the GNU General Public License
; *   along with this program; if not, write to the Free Software
; *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
; *
; **********************************************************************


BPB_FAT    struc

BS_OEMName      db  8 dup(?)    ; OEM label

BPB_BytsPerSec 	dw ?            ; Number of bytes per sector (512) Must be one of 512, 1024, 2048, 4096.
BPB_SecPerClus 	db ?            ; Number of sectors per cluster Must be one of 1, 2, 4, 8, 16, 32, 64, 128.
BPB_RsvdSecCnt 	dw ?            ; reserved sectors, in 12/16 usually 1 for BPB, FAT32 uses 32
BPB_NumFATs    	db ?            ; number of FATs, 
BPB_RootEntCnt 	dw ?            ; root directory entries, 0 for FAT32. 512 is recommended for FAT16.
BPB_TotSec16   	dw ?            ; 16-bit total count of sectors on the volume, if 0 see BPB_TotSec32
BPB_Media       db ?            ; is no longer usually used, F8 HD FA Ram Disk
BPB_FATSz16    	dw ?            ; sectors per 1 FAT copy
BPB_SecPerTrk  	dw ?            ; sectors per track
BPB_NumHeads    dw ?            ; number of heads
BPB_HiddSec    	dd ?            ; hidden sectors
BPB_TotSec32   	dd ?            ; big total sectors  BPB_TotSec32 * BPB_BytsPerSec = HD size

BS_DrvNum      	db ?            ; boot unit
BS_Reserved1   	db ?            ; Reserved (used by Windows NT). FAT always 0
BS_BootSig     	db ?            ; 0x29 indicates next 3 fields in the boot sector present
BS_VolID       	dd ?            ; volume serial number
BS_VolLab      	db 11 dup(?)    ; volume label
BS_FilSysType  	db  8 dup(?)    ; filesystem id

BPB_FAT    ends


;ClusterBase             dw      ?          ; first sector of cluster # 2
;RootDirSeg              dw      ?          ; Selector for Directory's spot in memory
;RtDirBgSec              dw      ?          ; 1st sector of root directory
;RtDirSecCt              dw      ?          ; number of sectors taken up by directory

