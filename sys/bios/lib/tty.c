/****************************************************************************
 *
 *  tty.c -- Send charater to screen plus helper routines and serial output.
 *
 *  ========================================================================
 *
 *    Version 1.1       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Pulled and modified from linux kernel source and some
 *               based on http://www.brokenthorn.com code.
 *
 *               Simple screen and comm routines for early debug use
 *               after entering 32 bit pmode and before IDT is setup
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
/*
 * arch/x86/boot/tty.c - newer version of the kernel (5.1.X)
 *
 * Very simple screen and serial I/O
 * 
 * -----------------------------------------------------------------------
 * Protos are in boot.h
 * Functions:
 *     putchar
 *     puts
 *     early_setcolor
 *     early_gotoxy
 *     early_clearscreen
 *
 * Do not use these yet:
 *     getchar
 *     kbd_flush
 *     getchar_timeout
 */

// !!! Note keyboard code not working yet !!!

#include <stdinit.h>
#include "boot.h"
#include "portio.h"

// control output  0 - screen  1 - serial
int early_serial_base;

#define XMTRDY          0x20

#define TXR             0       /*  Transmit register (WRITE) */
#define LSR             5       /*  Line Status               */

// Note: Some systems may require 0xB0000 Instead of 0xB8000
// We dont care for portability here, so pick either one
#define VID_MEMORY	0xB8000

// these vectors together act as a corner of a bounding rect
// This allows GotoXY() to reposition all the text that follows it
static unsigned int _xPos=0;
static unsigned int _yPos=0;

static unsigned _startX=0;
static unsigned _startY=0;

static unsigned _scrnwidth=80;

// current color
static unsigned _color=0x07;

#pragma code_seg ( _TEXT );
 
static void serial_putchar(int ch)
{
	unsigned timeout = 0xffff;

	while ((inb(early_serial_base + LSR) & XMTRDY) == 0 && --timeout)
		cpu_relax();

	outb(ch, early_serial_base + TXR);
}

static void early_putchar(unsigned char c) 
{
//    if (c==0) return;

    /* start new line */
    if (c == '\n'||c=='\r'||(_xPos > (_scrnwidth-1))) {        
        _yPos+=1;                    // move down a row
        _xPos=_startX;               // move horz pos 0
        return;
    }

    /* draw the character */
    unsigned char* p = (unsigned char*)VID_MEMORY + (_xPos++)*2 + _yPos * (_scrnwidth*2);
    *p++ = c;
    *p =_color;
}

void putchar(int ch)
{
    // print to screen
    early_putchar(ch);
 
    // send to com port
    if (early_serial_base != 0) serial_putchar(ch);
}

void puts(const char *str)
{
	while (*str) 
		putchar(*str++);
}

unsigned early_setcolor(uint8_t backcolor, uint8_t forecolor) 
{
	_color = (backcolor << 4) && forecolor;
	return forecolor;
}

void early_gotoxy(unsigned x, unsigned y) 
{
    // reposition starting vectors for next text to follow
    // multiply by 2 do to the video modes 2byte per character layout
    _xPos = x*2;
    _yPos = y*2;
    _startX=_xPos;
    _startY=_yPos;
}

void early_clearscreen(void) 
{
    unsigned int clr_word = ((_color << 8) | 0x20);  

    _asm {
        xor    edi, edi
        mov    edi, VID_MEMORY
        mov    eax, clr_word
        xor    ecx, ecx
        mov    ecx, 0x960
        rep stosw
    }

    // go to start of previous set vector
    _xPos=_startX;
    _yPos=_startY;
}

/*
 * Read from the keyboard
 */
 
/* INT 16,0 - Wait for Keypress and Read Character
 * AH = 00 
 * 
 * on return:
 * AH = keyboard scan code
 * AL = ASCII character or zero if special function key
 * 
 * - halts program until key with a scancode is pressed
 * - see  SCAN CODES - http://stanislavs.org/helppc/int_16-0.html
 */
int getchar(void);
#pragma aux getchar = \
    "mov    eax, 0x00" \
    "int 0x16" \
    "and    eax, 0xFF" \
    modify [ eax ] \
    value [ eax ];

/* INT 16,1 - Get Keyboard Status
 * AH = 01
 * on return:
 * ZF = 0 if a key pressed (even Ctrl-Break)
 * AX = 0 if no scan code is available
 * AH = scan code
 * AL = ASCII character or zero if special function key

 * - data code is not removed from buffer
 * - Ctrl-Break places a zero word in the keyboard buffer but does
 * register a keypress.
 */
static int kbd_pending(void);
#pragma aux kbd_pending = \
    "mov    eax, 0x01" \
    "int 0x16" \
    "pushf" \
    "pop    eax" \
    "and    eax, 0x40" \
    modify [ eax ] \
    value [ eax ];
    
/*
 * Read the CMOS clock through the BIOS, and return the
 * seconds in BCD.
 */

/* INT 1A,2 - Read Time From Real Time Clock (XT 286,AT,PS/2)
 * AH = 02
 * 
 * on return:
 * CF = 0 if successful
 *    = 1 if error, RTC not operating
 * CH = hours in BCD
 * CL = minutes in BCD
 * DH = seconds in BCD
 * DL = 1 if daylight savings time option
 * 
 * - on AT with BIOS before 6/10/85, DL is not returned
 */
static uint8_t gettime(void);
#pragma aux gettime = \
    "mov    ah, 0x02" \
    "int 0x1A" \
    modify [ eax edx ecx ] \
    value [ dh ];
    
void kbd_flush(void)
{
	for (;;) {
		if (!kbd_pending())
			break;
		getchar();
	}
}

int getchar_timeout(void)
{
	int cnt = 30;
	int t0, t1;

	t0 = gettime();

	while (cnt) {
		if (kbd_pending())
			return getchar();

		t1 = gettime();
		if (t0 != t1) {
			cnt--;
			t0 = t1;
		}
	}

	return 0;		/* Timeout! */
}

/* debug stuff */
int getxpos(void)
{
	return _xPos;
}

int getypos(void)
{
	return _yPos;
}

int startxpos(void)
{
	return _startX;
}

int startypos(void)
{
	return _startY;
}




#pragma code_seg ( );
