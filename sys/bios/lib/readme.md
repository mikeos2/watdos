**Startup library**

Misc routines that I pulled from various sources over the years, mostly from the Linux startup code. I am having too many problems trying to link in a Watcom lib, so these sources are stand alone.  
   
The following are the calls provided by the library routines. All prototypes are in the boot.h file.   

```
static void cpu_relax(void);
```

Used to induce a delay. It was a rep;nop; (2 byte delay), changed to pause.   

```
static inline void outb(uint8_t v, uint16_t port)
static inline uint8_t inb(uint16_t port)
static inline void outw(uint16_t v, uint16_t port)
static inline uint16_t inw(uint16_t port)
static inline void outl(uint32_t v, uint16_t port)
static inline uint32_t inl(uint32_t port)
```
(Linux description) This family of functions is used to do low-level port input and output.  The out* functions do port output, the in* functions do port input; the b-suffix functions are byte-width and the w-suffix functions word-width. They are primarily designed for internal kernel use.

outb() and friends are hardware-specific.  The value argument is passed first and the port argument is passed second, which is the opposite order from most DOS implementations.


***printf.c***
```
int sprintf(char *buf, const char *fmt, ...);
int vsprintf(char *buf, const char *fmt, va_list args);
int printf(const char *fmt, ...);
```

***string.c***
```
int strcmp(const char *str1, const char *str2);
size_t strnlen(const char *s, size_t maxlen);
unsigned int atou(const char *s);
```
**tty.c**
```
void puts(const char *);
void putchar(int);
int getchar(void);
void kbd_flush(void);
int getchar_timeout(void)
```



