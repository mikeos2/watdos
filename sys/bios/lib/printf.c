/****************************************************************************
 *
 *  printf.c -- Early printf for pmode
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Early printf function for pmode debuging
 * 
 *  Ref: http://www.brokenthorn.com/Resources/OSDevPic.html
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

/*
 * os2ldr -- printf.c
 *
 * Modified for use with Open Watcom - M Greene 25 June 2008
 *   - Moved do_div macro into number( )
 * 
 * Modified and recycled for use with Watdos Project - M Greene April 2019
 */

/* -*- linux-c -*- ------------------------------------------------------- *
 *
 *   Copyright (C) 1991, 1992 Linus Torvalds
 *   Copyright 2007 rPath, Inc. - All Rights Reserved
 *
 *   This file is part of the Linux kernel, and is made available under
 *   the terms of the GNU General Public License version 2.
 *
 * ----------------------------------------------------------------------- */

/*
 * arch/i386/boot/printf.c - old version of the kernel
 *
 * Oh, it's a waste of space, but oh-so-yummy for debugging.  This
 * version of printf() does not include 64-bit support.  "Live with
 * it."
  * -----------------------------------------------------------------------
 * Protos are in boot.h
 * Functions:
 *     sprintf
 *     vsprintf
 *     printf
 */

#include <stdarg.h>
#include <stdint.h>

#define PUTSIN 1
#include "tty.h"

#include "ctype.h"
#include "string.h"


#pragma code_seg ( _TEXT );

static int skip_atoi(const char **s)
{
        int i = 0;

        while (isdigit(**s))
                i = i * 10 + *((*s)++) - '0';
        return i;
}

#define ZEROPAD 1               /* pad with zero */
#define SIGN    2               /* unsigned/signed long */
#define PLUS    4               /* show plus */
#define SPACE   8               /* space if plus */
#define LEFT    16              /* left justified */
#define SPECIAL 32              /* 0x */
#define LARGE   64              /* use 'ABCDEF' instead of 'abcdef' */


// MKG WATCOM -- macro was giving problems so moved into function
/*
#define do_div(n,base) ({ \
int __res; \
__res = ((unsigned long) n) % (unsigned) base; \
n = ((unsigned long) n) / (unsigned) base; \
__res; })
  */

//static char *number(char *str, long num, int base, int size, int precision, int type)
static char *number(char *str, int64_t num, uint8_t base, int8_t size, int8_t precision, int8_t type)
{
    char c;
    char sign;
    char tmp[66];
    const char *digits = "0123456789abcdefghijklmnopqrstuvwxyz";
    int i;
    
    if (type & LARGE) digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    if (type & LEFT) type &= ~ZEROPAD;
    if (base < 2 || base > 36) return 0;
        
    c = (type & ZEROPAD) ? '0' : ' ';
        
    sign = 0;
       
    if (type & SIGN) {
        if (num < 0) {
            sign = '-';
            num = -num;
            size--;
        } else if (type & PLUS) {
                   sign = '+';
                   size--;
               } else if (type & SPACE) {
                          sign = ' ';
                          size--;
                      }
    }
    
    if (type & SPECIAL) {
        if (base == 16) size -= 2;
        else if (base == 8) size--;
    }
    
    i = 0;
// ==> Here   https://stackoverflow.com/questions/51708273/how-can-i-do-64-bit-division-on-linux-kernel   
    if (num == 0) tmp[i++] = '0';
    else while (num != 0) {
             // tmp[i++] = digits[do_div(num, base)];
             // MKG do_div macro moved to here, just was easier
             //tmp[i++] = digits[(((unsigned long) num) % (unsigned) base)];
             //num = ((unsigned long) num) / (unsigned) base;  // shift right
             tmp[i++] = digits[(((uint64_t) num) % (uint8_t) base)];
             num = ((uint64_t) num) / (uint8_t) base;
         }
        
    if (i > precision) precision = i;
    size -= precision;
    
    if (!(type & (ZEROPAD + LEFT))) while (size-- > 0) *str++ = ' ';
    if (sign) *str++ = sign;
    if (type & SPECIAL) {
        if (base == 8) *str++ = '0';
        else if (base == 16) {
                 *str++ = '0';
                 *str++ = digits[33];
             }
    }
    if (!(type & LEFT)) while (size-- > 0) *str++ = c;
        
    while (i < precision--) *str++ = '0';
    while (i-- > 0) *str++ = tmp[i];
    while (size-- > 0) *str++ = ' ';

    return str;
}

/* I am pretty sure I pulled this from Linux source years ago. I also think
 * it is for pre-protected mode use and barfs on 64bit numbers. I edited and 
 * commented to figure it and fix.
 * 
 * buf - array of char elements where the resulting string is to be stored
 * 
 * fmt - string that contains the text to be written to the buf, can optionally 
 *       contain embedded format tags that are replaced by the values specified 
 *       in subsequent additional arguments [args]
 * 
 * args - arguments and are formatted as requested. Format tags prototype −
 *            %[flags][width][.precision][length]specifier
 */
int vsprintf(char *buf, const char *fmt, va_list args)
{
    int32_t len;
    //uint32_t num;
    uint64_t num;
    int32_t i;
    int32_t base;
    char *str;
    const char *s;

    int32_t flags;              /* flags to number() */
    int32_t field_width;        /* width of output field */
    int32_t precision;          /* min. # of digits for integers; max
                                   number of chars for from string */
    int32_t qualifier;          /* 'h', 'l', or 'L' for integer fields */

    // this goes through the string and finds embedded format tags
    for (str = buf; *fmt; ++fmt) {

        // skip if not format identifer
        if (*fmt != '%') {
            *str++ = *fmt;
            continue;
        }

        // format identifer found - process

        /* process flags */
        flags = 0;
        
    repeat:
        ++fmt;          /* this also skips first '%' */
        
        switch (*fmt) {
            case '-':    // Left-justify within the given field width
                    flags |= LEFT;
                    goto repeat;
            case '+':    // Forces to precede the result with a plus or minus sign
                    flags |= PLUS;
                    goto repeat;
            case ' ':    // If no sign is going to be written, a blank space is inserted before the value
                    flags |= SPACE;
                    goto repeat;
            case '#':    // Used with o, x or X specifiers the value is preceded with 0, 0x or 0X 
                         // Used with e, E and f, it forces the written output to contain a decimal point even if no digits would follow. 
                         // Used with g or G the result is the same as with e or E but trailing zeros are not removed.
                    flags |= SPECIAL;
                    goto repeat;
            case '0':    // Left-pads the number with zeroes (0) instead of spaces,
                    flags |= ZEROPAD;
                    goto repeat;
        }  // no more flags - exit switch

        /* get field width */
        field_width = -1;
        
        // (number) Minimum number of characters to be printed.
        // * The width is not specified in the format string, but as an 
        //   additional integer value argument preceding the argument that has to be formatted.
        if (isdigit(*fmt)) field_width = skip_atoi(&fmt);
        else if (*fmt == '*') {
                 ++fmt;
                 /* it's the next argument */
                 field_width = va_arg(args, int);
                 if (field_width < 0) {
                     field_width = -field_width;
                     flags |= LEFT;
                 }
             }

        /* get the precision */
        precision = -1;
        
        if (*fmt == '.') {
            ++fmt;
            
            if (isdigit(*fmt)) precision = skip_atoi(&fmt);
            else if (*fmt == '*') {
                     ++fmt;
                     /* it's the next argument */
                     precision = va_arg(args, int);
                 }
            
            if (precision < 0) precision = 0;
        } // end of precision

        /* get the conversion qualifier - Length & Description */
        qualifier = -1;
        
        if (*fmt == 'h' || *fmt == 'l' || *fmt == 'L') {
            qualifier = *fmt;
            ++fmt;
        }

        /* default base - Specifier & Output */
        base = 10;

        switch (*fmt) {
            case 'c':  // Character
                    if (!(flags & LEFT)) while (--field_width > 0) *str++ = ' ';
                    *str++ = (unsigned char)va_arg(args, int);
                    while (--field_width > 0) *str++ = ' ';
                    continue;

            case 's':  // String of characters
                    s = va_arg(args, char *);

                    len = strnlen(s, precision);

                    if (!(flags & LEFT)) while (len < field_width--) *str++ = ' ';
                    for (i = 0; i < len; ++i) *str++ = *s++;
                    while (len < field_width--) *str++ = ' ';
                    continue;

            case 'p':  // Pointer address
                    if (field_width == -1) {
                        field_width = 2 * sizeof(void *);
                        flags |= ZEROPAD;
                    }
                    str = number(str, (unsigned long)va_arg(args, void *), 16, field_width, precision, flags);
                    continue;

            case 'n':  // Nothing printed
                    if (qualifier == 'l') {
                        long *ip = va_arg(args, long *);
                        *ip = (str - buf);
                    } else {
                        int *ip = va_arg(args, int *);
                        *ip = (str - buf);
                    }
                    continue;

            case '%':  // Character
                    *str++ = '%';
                    continue;

            /* integer number formats - set up the flags and "break" */
            
            case 'o':  // Signed octal
                    base = 8;
                    break;

            // X and x - both base 16
            case 'X':  // Unsigned hexadecimal integer (capital letters)
                    flags |= LARGE;
            
            case 'x':  // Unsigned hexadecimal integer
                    base = 16;
                    break;

            case 'd':  // Signed decimal integer
            case 'i':  // Signed decimal integer
                    flags |= SIGN;
            case 'u':  // Unsigned decimal integer
                    break;

            default:
                   *str++ = '%';
                   if (*fmt) *str++ = *fmt;
                   else --fmt;
                   continue;
        }  // end of Specifier & Output switch

        if (qualifier == 'l') num = va_arg(args, unsigned long);
        else if (qualifier == 'h') {
                 num = (unsigned short)va_arg(args, int);
                 if (flags & SIGN) num = (short)num;
             } else if (flags & SIGN) num = va_arg(args, int);
                    else num = va_arg(args, unsigned int);

        str = number(str, num, base, field_width, precision, flags);

    }  // end of find embedded format tags 'for' statement
    
    /* Not supported:
     * e - Scientific notation (mantissa/exponent) using e character
     * E - Scientific notation (mantissa/exponent) using E character
     * f - Decimal floating point
     * g - Uses the shorter of %e or %f
     * G - Uses the shorter of %E or %f
     */

    *str = '\0';

    return str - buf;
}

int sprintf(char *buf, const char *fmt, ...)
{
        va_list args;
        int i;

        va_start(args, fmt);
        i = vsprintf(buf, fmt, args);
        va_end(args);

        return i;
}

int printf(const char *fmt, ...)
{
        char printf_buf[1024];

        va_list args;
        int printed;

        va_start(args, fmt);
        printed = vsprintf(printf_buf, fmt, args);
        va_end(args);

        puts(printf_buf);

        return printed;
}

#pragma code_seg ( );
