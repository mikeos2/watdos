/****************************************************************************
 *
 *  string.c -- String routines.
 *
 *  ========================================================================
 *
 *    Version 1.1       Michael K Greene <mikeos2@gmail.com>
 *                      May 2019
 * 
 *  ========================================================================
 *
 *  Description: Pulled and modified from linux kernel source and some
 *               based on http://www.brokenthorn.com code.
 *
 *               Simple string routines for early debug use
 *               after entering 32 bit pmode and before IDT is setup
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
/*
 * os2ldr -- string.c
 *
 *    Version 1.0       Michael K Greene <mike@mgreene.org>
 *                      July 2008 
 *
 * Modified for use with Open Watcom - M Greene 25 June 2008
 *   - Moved do_div macro into number( )
 * 
 * Pulled up for WatDos project - May 2019
 */

/* -*- linux-c -*- ------------------------------------------------------- *
 *
 *   Copyright (C) 1991, 1992 Linus Torvalds
 *   Copyright 2007 rPath, Inc. - All Rights Reserved
 *
 *   This file is part of the Linux kernel, and is made available under
 *   the terms of the GNU General Public License version 2.
 *
 * ----------------------------------------------------------------------- */

/*
 * arch/i386/boot/string.c - old version of the kernel
 *
 * Very basic string functions
 * 
 * -----------------------------------------------------------------------
 * Protos are in boot.h
 * Functions:
 *     strcmp
 *     strnlen
 *     atou
 */

#include <stddef.h>
#include "boot.h"
#include "ctype.h"

#pragma code_seg ( _TEXT );

int strcmp(const char *str1, const char *str2)
{
    const unsigned char *s1 = (const unsigned char *)str1;
    const unsigned char *s2 = (const unsigned char *)str2;
    int delta = 0;

    while (*s1 || *s2) {
        delta = *s2 - *s1;
        if (delta)
            return delta;
            s1++;
            s2++;
    }
    return 0;
}

size_t strnlen(const char *s, size_t maxlen)
{
    const char *es = s;
    while (*es && maxlen) {
        es++;
        maxlen--;
    }

    return (es - s);
}

unsigned int atou(const char *s)
{
    unsigned int i = 0;
    while (isdigit(*s))
                i = i * 10 + (*s++ - '0');
    return i;
}

#pragma code_seg ( );
