 /****************************************************************************
 *
 *  ldrinit.c -- loader toplevel C module.
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 * 
 *  ========================================================================
 *
 *  Description: Start of C-Code for watbios
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 *  ======================================================================== 
 * 
 * On entry to main the following conditions exist:
 * 
 * 1. The system is in protected mode with interrupts off
 * 2. Real mode IVT and BDA are at 0x90000
 * 3. RMTableData holds all real mode data being passed (rmdata.h)
 *
 ***************************************************************************/

/* this is the C entry point for WatBIOS -

  I need to come up with a generic filesystem system like OS/2.  All I want 
  passed to watbios is the call vectors so file i/o is a blackbox
 */

#include "printf.h"
#include "tty.h"
#include "bootparams.h"
#include "rmdata.h"

/* Declare the real mode data table here -- see rmdata.h
 * Note - if declared in watinit.asm C-code tried to read
 * each element as a long. Could be something on my end,
 * but this works for now.
 */
realmode_data RMTableData; 
uint16_t RMTabSize = sizeof(RMTableData);

// memory found in memory.c
uint16_t  LoMem = 0;                 // !!!!! removing causes alignment 
uint16_t  HiMem = 0;                 // problem --- WTF
uint32_t  ExMem = 0;

struct ScreenStatus *scnstat;

extern uint8_t pic_status (uint8_t picNum);
extern uint8_t pic_imr(uint8_t picNum);
extern uint32_t IDT_get_base( );
extern uint32_t GDT_get_base( );
extern void system_init(realmode_data RMTableData);
extern void print_biosmap(PHYMEMMAP *);

// **!! main structure holding boot info, much like Linux zero page !!**//
struct bootparams boot_params;


void main( void )
{
    early_clearscreen( );
    printf("SPARTACUS  OS/2019  HARDSTART BUILD    November 2019\n");
    system_init(RMTableData);  // processes system data


    /***** Debug --- print out system data *****/
    // the following dumps system data
    printf("PICs relocated and initialized: %d  %d  %d  %d\n", 
           pic_status(0), pic_imr(0), pic_status(1), pic_imr(1));
    printf("IDT %x  GDT %x  IODelay %d\n", IDT_get_base(), GDT_get_base(), RMTableData.io_count);
    printf("INT11 %X  15C0  %X\n", RMTableData.INT11RET, RMTableData.INT15C0Size);

    int count = 0;
    for(uint8_t i = RMTableData.INT15C0Size; i > 0; i--) {
        printf("%d:%X ",count, RMTableData.INT15C0Data[count++]);  
    }
    printf("\n");
    printf("VESA: %d.%d\n", RMTableData.VESAMajor, RMTableData.VESAMinor);

    print_biosmap(&boot_params.PhyMemMap);  // pulls from bootprams structure
            
loop:
    goto loop;
    
    return;
}
