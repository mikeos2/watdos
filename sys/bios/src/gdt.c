/****************************************************************************
 *
 *  gdt.c -- GDT routines and variables
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Global Descriptor Table
 * 
 *  Ref: http://www.brokenthorn.com/Resources/
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#include <stdinit.h>
#include "gdt.h"
#include "printf.h"
#include "tty.h"


static struct gdt_ptr_struct  GDTTable = {0,0};


/* Initialize GDT values passed in from loader
 * size and linear location
 */
void gdt_init(uint16_t limit, uint32_t base)
{
    GDTTable.limit = limit;
    GDTTable.base = base;

    return;
}

uint32_t GDT_get_base( void)
{
    return GDTTable.base;
}

/* Return current GDT values 
 * size and linear location
 */
void gdt_get(gdt_ptr_t *GDT_Table)
{
    GDT_Table->limit = GDTTable.limit;
    GDT_Table->base  = GDTTable.base;

    return;
}
