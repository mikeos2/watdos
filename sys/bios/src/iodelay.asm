;**************************************************************************
;
;  iodelay.asm
;
;  Description: Calculate io delay based on number of loops
;
;  Very old, from an old loader project (circa 2008ish)- try to add to 
;  WatBIOS in case I need it -- MKG November 2019
;
;  Michael K Greene <mike@mgreene.org>
;
;  This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 2 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program; if not, write to the Free Software
;   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;==========================================================================
;
;  - Times number of loops (105588) using PIT counter 0.
;
;  - Timer operations: 1. set to start (T0)
;                      2. get start time (T1)
;                      3. run loops
;                      4. get end time (T2)
;
;  - Get interval between start and end time, correct based
;    on set to start -- (T2 - T1) - T0 (refcount)
;
;  - Each counter tick is 839ns so 105588 (loops)/839 results in
;    number of loops per tick
;
;  Note: After a lot of head scrtatching and running numbers through
;  a spread sheet, I found that the result was approx 1.678 ratio to
;  the above. So, it seems the result is corrected to approx 500ns
;  839/1.678. I have no idea how acurate this value must be, but
;  remainders were rounded up therefore I assume the 500ns is a
;  minimum.
;
;  - Given the above 105588 (loops) * 500 / 839 gets the iodelay
;
;  Might needs some more work, but is working on my systems. Keep storing
;  T1 and T2 values for bug check.
;
;**************************************************************************


include segments.inc
include timer.inc


    public  FindIODelay

TMPDELAYNUM     equ     0x0400

;_BSS segment

    extrn  _io_count:word
    extrn  _count_t1:word
    extrn  _count_t2:word

;_BSS ends

.686
; ***** TEXT ***********************************************
_TEXT segment


; TmpIODelay
;
; After reading a bit I know this is a delay to let the PIT
; settle between reads and writes
;
; I just kept this as I saw via Bochs, however, I did see a
; number of examples that used "jmp  $+2". Freeldr used a
; loop of 256.
;
; This might have to be altered based on system speed??
;
; 3 Jul 08 - running on a real system seems to result in an almost
; constant result. However, running on Virtual PC results in large
; differences.

TmpIODelay  proc near

    push    eax
    mov     ax, TMPDELAYNUM  ; see note above

_tmpio_delay_loop:
    dec     ax
    jnz    _tmpio_delay_loop

    pop    eax
    ret

TmpIODelay  endp


; FindIODelay
;
; Used Bochs output and periphs.asm (freeldr) to figure the following.
; FREELDR Ref: NB: The routines _FindIODelay and _IODelay are taken from
; Frank van Gilluwe, *The Undocumented PC*, 2d. ed., pp 42-44.

FindIODelay  proc near

    ; 11/5/19 - do not need to save anything
    ; save registers and segs
    ;pusha
    ;push   ds
    ;push   cs
    ;pop    ds
    ;pushf               ; save flags
    ;cli                 ; stop interupts


    ; *** Setup counter 0 -- T0
    xor    ebx, ebx       ; zero bx, will be count of 65536

    ; This routine sets counter 0 of the 8254 chip to interrupt
    ; in mode 2 18.2 times per second -- from freeldr comment

    ; setup PIT 0  0x34  00110100b
    ; Bits 7-6: 00 : Select Counter (0-2)
    ; Bits 5-4: 11 : Command: (11) means R/W 7-0, then 15-8
    ; Bits 3-1: 010 : Mode: (010) means rate generator (divide by n)
    ; Bit 0: 0: Binary
    mov    al, SC_CNT0+RW_LSBMSB+CM_MODE2+CW_BIN
    out    PORT_CW, al
    call   TmpIODelay

    ; In the next stanza we write 0 twice to port 40h.  This has the effect
    ; of writing the 16-bit word 0, which the PIT chip interprets as the
    ; value 64k.  Since the clock tick is 1.193180 MHz, the PIT chip will
    ; then cause an IRQ0 about every 56ms, or about 18.2 times per second.
    ;
    ; MKG:
    ; 8254 base clock frequency of 1193180Hz
    ; 1/f = T so 838ns @ tick
    ; counter 2^16 = 65536 so max count 55ms
    ;
    ; Simple walkthrough:
    ; - First step is to setup and start counter 0 - while this is a start point
    ;   I'll call it Ref (bx) [above]
    ; - Next setup and start counter with result Start in cx
    ; - Run a fixed loop
    ; - Get the counter0 ticks with result End in dx

    mov    ax, bx
    out    PORT_CNT0, al
    call   TmpIODelay
    mov    al, ah
    out    PORT_CNT0, al
    call   TmpIODelay


    ; *** timer setup and read to cx -- T1 ***
    ; setup PIT 0  0x00
    mov    al, SC_CNT0+RW_LATCHCNT+CM_MODE0+CW_BIN
    out    PORT_CW, al
    call   TmpIODelay
    in     al, PORT_CNT0        ; read LSB into cl and MSB into ch
    call   TmpIODelay
    mov    cl, al
    in     al, PORT_CNT0
    mov    ch, al


    ; *** delay loop ***
    ; runs 105588 loops while couter 0 counts
    mov    esi, 0x19C74   ; 105588
_delayloop:
    dec    esi
    jnz    _delayloop


    ; *** timer setup and read to dx -- T2 ***
    ; setup PIT 0
    mov    al, SC_CNT0+RW_LATCHCNT+CM_MODE0+CW_BIN
    out    PORT_CW, al
    call   TmpIODelay
    in     al, PORT_CNT0        ; read LSB into dl and MSB into dh
    call   TmpIODelay
    mov    dl, al
    in     al, PORT_CNT0
    mov    dh, al

    popf                ; restore flags

    ; *** calculate io_count ***
    ; remeber counter couts down so results are negative

    ; T0 -- bx -- zero
    ; T1 -- cx
    ; T2 -- dx
    mov    DGROUP:_count_t1, cx
    mov    DGROUP:_count_t2, dx

    ; get time values
    sub    bx, cx       ; (0 - (-cx) = bx
    sub    cx, dx       ; ((-cx) - (-dx)) = cx

    ; bx contains diff between T0 ref and T1 start
    ; cx contains total time interval between T1 and T2

    ; check for a diffrence between T0 and T1, the result will
    ; be the value expected for timer setup and overhead

    and    bx, bx       ; diff between T0 and T1 zero ? But I would guess
                        ; the diff between T0 abd T1 should never be zero
    jz     _chkinterval ; yes - jmp - damn fast system or something wrong

    sub    cx, bx       ; interval compensate for setup ticks
                        ; (TmpIODelay, etc... ?) (cx - bx) = cx
_chkinterval:
    ; a zero here will get us a DIV by Zero
    ; but really, if we get T1 to T2 zero then something
    ; has got to be wrong
    or     cx, cx
    jz     _iocnterr

    ; have to handle what ifs - T2 is equal to or below T1
    ; how's this going to happen unless it wraps maybe?
    ; who cares -- just max out and see what happens.
    cmp    cx, bx
    jbe   _iocnterr

    ; 0x19C74 (105588) loops / corrected count (bx)
    mov    dx, 1
    mov    ax, 0x9C74
    div    cx
    or     dx, dx       ; remainder? round up
    jz     _no_remainder1
    inc    ax

_no_remainder1:

    ; ((result above) * 500) / 839
    mov    bx, 0x1F4
    mul    bx
    mov    bx, 0x347
    div    bx
    or     dx, dx       ; remainder? round up
    jz     _io_set
    inc    ax
    jmp    _io_set

_iocnterr:
    ; something is wrong with the timer so just max-out
    ; the delay - 0xFFFF (65535)
    mov    ax, 0xFFFF
    jmp    _io_set

_io_set:
    mov    DGROUP:_io_count, ax

    ; restore registers and segs
    pop    ds
    popa
    ret

FindIODelay  endp

_TEXT ends

end

