;****************************************************************************
; *
; *  watinit.asm -- Development BIOS
; *
; *  ========================================================================
; *
; *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
; *                      April-July 2019
; *
; *  ========================================================================
; *
; *  Description: Loaded at 0x10000 by the watload. 
; *
; *  ========================================================================
; *
; *   This program is free software; you can redistribute it and/or modify
; *   it under the terms of the GNU General Public License as published by
; *   the Free Software Foundation; either version 2 of the License, or
; *   (at your option) any later version.
; *
; *   This program is distributed in the hope that it will be useful,
; *   but WITHOUT ANY WARRANTY; without even the implied warranty of
; *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; *   GNU General Public License for more details.
; *
; *   You should have received a copy of the GNU General Public License
; *   along with this program; if not, write to the Free Software
; *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
; *
; ***************************************************************************
; *
; * This is the stub on entry from watload. The link file orders the 
; * segments with BIOSinitEntry at start. This should bounce us to
; * main, the start of C code.
; *
; ***************************************************************************
; * 
; * This is the initial code after hand-off from the loader. At this jump
; * the system is in 32-bit protected mode with interrupts off. We start 
; * by setting up the descriptors and stack.
; *
; * Changed November 2019:
; * All real mode data is passed from the loader to watbios via the
; * RMTableData structure defined in rmdata.h whoses location is 
; * passed in di/si.
; * 
; * Registers are cleared as is normal for a known starting value.
; * 
; * The IDT will start at 0x00, so for right now move the IVT to an
; * out of the way location at 0x90000. Clear the old IVT area with
; * 0x00 and jump to the start of C-code.
; *
; ***************************************************************************
; Default layout at entry
;  ########  0xA0000
; |        |
; |        | 0x90000 copy of real mode IVT and BDA
; |        |
; ~        ~
; |        |
; |--------| 
; |watbios | 
; |        | 
; |--------| 0x10000  Watload places watbios here
; |        |
; ~        ~
; |        | 
; |--------| 0000:0B00
; |        |  Mem Map    After memmap processing, this chunk free
; |--------| 0000:0900 
; |  GDT   | 
; |********| 0000:0800
; |        | 
; |        |
; |  IDT   | Interrupt Descriptor Table
; +--------+ 0000:0000
; **********************************************************************

.686p
.mmx
.model flat

include segments.inc
include rmdata.inc

    public    _cstart_
    extern    main:near
    extern    RMTableData:near
    extern    RMTabSize:near
        
; ********************************************************************
; Entry from watload module
; ********************************************************************
BIOSinitEntry    segment 

_cstart_:

    cli  
    mov    eax, 0x10
    mov    ds, eax             ; Load DS with descriptor 2 info
    mov    es, eax             ; Load ES with descriptor 2 info
    mov    fs, eax             ; Load FS with descriptor 2 info
    mov    gs, eax             ; Load GS with descriptor 2 info
    mov    ss, eax             ; Load SS with descriptor 2 info 
    xor    eax, eax 
    mov    eax, _StackTop          
    mov    esp, eax            ; stack is set

    ; copy real mode data table    
    and    esi, 0xFFFF
    and    edi, 0xFFFF
    mov    eax, edi
    shl    eax, 0x04
    add    eax, esi
    mov    esi, eax
    mov    edi, offset RMTableData
    mov    ecx, dword ptr [RMTabSize]
    rep    movsb    
    
    xor    ebx, ebx
    xor    ecx, ecx
    xor    edx, edx
    xor    edi, edi
    xor    esi, esi
    xor    ebp, ebp        

    jmp    init_asm
    
BIOSinitEntry    ends        

_TEXT    segment 

init_asm:
    ; for right now, save IVT and BDA at 0x90000
    mov    esi, 0x00000
    mov    edi, 0x90000
    mov    ecx, 0x500
    rep    movsb

    ; clear IDT table with 0x00
    xor    edi, edi
    mov    di, [RMTableData.IDTAddr]    
    mov    eax, 0x00
    xor    ecx, ecx
    mov    cx, RMTableData.GDTAddr 
    rep	stosb

    jmp    main

_TEXT    ends

_DATA    segment


_DATA    ends

; BSS ****************************************************************
_BSS segment

_BssStart    equ    $ ; start of BSS marker for checks

_BSS ends

; STACK **************************************************************

;public _StackBottom, _StackTop

StackSig   equ   0x474D

STACK segment

align 4
     
    ; from disassembly the following needs to be dword to stuff
    ; stack bottom signature
    _StackBottom   dw      StackSig
                   dw      0x1000 dup (0x00)
    _StackTop      equ     $

STACK ends


END _cstart_ 

