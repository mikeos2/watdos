/****************************************************************************
 *
 *  pic8259.c -- 8259 PIC routines.
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: 8259 PIC routines
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
 
 #include "portio.h"
 #include "picdef.h"
 
 // Setup and relocate params
#define ICW1                       (ICW1_MCS_3 | ICW1_MCS_2 | ICW1_MCS_1 | ICW1_INIT|  \
                                    ICW1_EDGE_TRIG | ICW1_INTERVAL8 | ICW1_CASCADE|  \
                                    ICW1_ICW4_NEEDED)

#define ICW2_PIC1_BASE_MASTER      (PIC1_BASE)
#define ICW2_PIC2_BASE_SLAVE       (PIC2_BASE)

#define ICW3_MASTER_IRQ            (1 << PIC_SLAVE_IRQ)
#define ICW3_SLAVE_IRQ             (PIC_SLAVE_IRQ)

#define ICW4                       (ICW4_NOT_SPEC_FULLY_NESTED | ICW4_NON_BUF_MODE | \
                                    ICW4_NORM_EOI | ICW4_8086_MODE)
 
void init_pics( void)
{
     /** PIC Setup ---- see picdef.h **/
    outb(ICW1, PIC1_COMMAND);                  // ICW 1 - Send Initialization Control Word (ICW)  1
    outb(ICW1, PIC2_COMMAND);
    outb(ICW2_PIC1_BASE_MASTER, PIC1_DATA);    // ICW 2 - have to send when IWC 1 resets PIC
    outb(ICW2_PIC2_BASE_SLAVE, PIC2_DATA);
    outb(ICW3_MASTER_IRQ, PIC1_DATA);          // ICW 3 must be sent when cascading set in ICW 1
    outb(ICW3_SLAVE_IRQ, PIC2_DATA);
    outb(ICW4, PIC1_DATA);                     // ICW 4 controls how everything is to operate
    outb(ICW4, PIC2_DATA);    
    outb(0x00, PIC1_DATA);                     // Null out the data registers
    outb(0x00, PIC2_DATA); 
}

uint8_t pic_status(uint8_t picNum)
{
    if (picNum > 1) return(2);
    
    uint8_t reg = (picNum==PIC2) ? PIC2_STATUS : PIC1_STATUS;

    return(inb(reg));
}

uint8_t pic_imr(uint8_t picNum)
{
    if (picNum > 1) return(2);
    
    uint8_t reg = (picNum==PIC2) ? PIC2_IMR : PIC1_IMR;

    return(inb(reg));
}

/* Source File: dhintr.asm 
;*              DHSendEOI        - send eoi to the PIC's for a particular irq *
;*              DHInitInterrupts - initialize int hardware and return tables  *
;*              DHSetIRQMask     - mask or unmask specified interrupt         *
;*              DHGetMask - Get 8259's interrupt mask                         *
;*              DHSetMask - Set 8259's interrupt mask                         *
;*              DHCallInt10 - Call INT 10h to get video service
*/

