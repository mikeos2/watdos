 /****************************************************************************
 *
 *  sysinit.c -- initalize system.
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      November 2019
 * 
 *  ========================================================================
 *
 *  Description: 
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 ***************************************************************************/

#include "printf.h"
#include "tty.h"
#include "bootparams.h"
#include "rmdata.h"
#include "pics.h"
#include "idt.h"
#include "gdt.h"
#include "memory.h"

extern realmode_data RMTableData;


void system_init( )
{
    init_pics( ); 
    
    idt_init(RMTableData.IDTAddr, (RMTableData.GDTAddr - RMTableData.IDTAddr));
    gdt_init(RMTableData.GDTAddr, 0x100);    // 0x100 default bootup size
    
    detect_memory( );       // build physical memory map of system

}
