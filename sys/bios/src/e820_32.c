/****************************************************************************
 *
 *  e820_32.c -- memory formating functions
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mike@mgreene.org>
 *                      July 2008
 *
 *  ========================================================================
 *
 *  Description: Pulled and modified from linux kernel source for use
 *               in os2ldr project.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

/* -*- linux-c -*- ------------------------------------------------------- *
 *
 *   Copyright (C) 1991, 1992 Linus Torvalds
 *   Copyright 2007 rPath, Inc. - All Rights Reserved
 *
 *   This file is part of the Linux kernel, and is made available under
 *   the terms of the GNU General Public License version 2.
 *
 * ----------------------------------------------------------------------- */

#include <i86.h>
#include <stdint.h>
#include "boot.h"
#include "ldrinit.h"
#include "memory.h"
#include "string.h"

#define PUTSIN  1    // early puts - not ow version
#include "tty.h"
#include "printf.h"  // tmp

// see ldrinit.c //
extern struct bootparams boot_params;

static void sort_e820_map(mem_meminfo_t *biosmap);

/* 10/14 -need following routines:
 * modify memory record
 * delete memory record
 * add memory record
 */

#pragma code_seg ( _TEXT );

/*
 * Sanitize an E820 e820 map - portions from current linux source
 *
 * Some e820 responses include overlapping entries.  The following
 * replaces the original e820 map with a new one.  Purpose:  sort the 
 * list, combine adjacent ranges of the same type, change any 
 * overlapping areas to the most restrictive type, and change any 
 * unrecognised "type" values to type 2.
 * 
 * Receive a mem_meminfo_t pointer of all memory data saved while in
 * real mode and pulled in memory.c (from memory.h):
 * 
 * typedef struct MEMINFO {
 * 	   uint16_t    int12_mem_k;
 *     uint32_t    ext_mem_k;
 *     uint32_t    alt_mem_k;
 *     uint16_t    e820_entries;
 *     e820biosrec_t e820_map[E820MAX];  // using 128 as default MKG 100819
 * };
 * typedef struct MEMINFO mem_meminfo_t;
 * 
 * Note: Some BIOS e820h interrupt returns overlapped memory mapping for
 * memory space, and it may not be sorted by address.
 *
 * So, sanitize_e820_map() sorts the map by address from low address
 * to high address and if overlapped, it selects memory map that
 * has the highest type of memory.
 *
 * Memory type is defined in memory.h
 * 
 */
int8_t sanitize_e820_map(mem_meminfo_t *biosmap)
{
    uint8_t  index1;
    
    // if there's only one memory region, don't bother - Linuxish leftover
    if (biosmap->e820_entries < 2) return -1;
    
    /* bail out if we find any unreasonable addresses in bios map - 
     * Linuxish leftover  MKG - as a note, size would have to be negitive 
     * for this check to find anything. I'm leaving it in because there 
     * has to be a case where this was needded -- overflow?
     *
     * !!Note: since this little check is going to run through the entire 
     * list, it seems like a good time to calculate the end address, so 
     * I at least feel like I'm not wasting time.
     */
    for (uint16_t i=0; i<biosmap->e820_entries; i++) {
       if (biosmap->e820_map[i].addr + biosmap->e820_map[i].size < biosmap->e820_map[i].addr) return -1;
       biosmap->e820_map[i].ends = biosmap->e820_map[i].addr + biosmap->e820_map[i].size - 1;
    }
    
    // Sort e820 memory by start address low to high
    sort_e820_map(biosmap);
	
    /*
     * ROM Area Standard usage of the ROM Area
	 * start        end         size      region/exception  description
     * 0x000A0000   0x000BFFFF  128 KiB   video RAM         VGA display memory
     * 0x000C0000   0x000C7FFF  32 KiB    (typically) ROM   Video BIOS
     * 0x000C8000   0x000EFFFF  160 KiB   (typically) ROMs  Mapped hardware & Misc.
     * 
     * 0x000F0000   0x000FFFFF  64 KiB    ROM               Motherboard BIOS
     */

    // I just want bean counting - create an entry for the 0xA0000 + video area 
    for(index1 = 0; index1 < biosmap->e820_entries-1; index1++) {
        if((biosmap->e820_map[index1].addr < 0xA0000) && (biosmap->e820_map[index1+1].addr > 0xA0000)) {
            biosmap->e820_map[biosmap->e820_entries].entry = biosmap->e820_entries;
            biosmap->e820_map[biosmap->e820_entries].addr = 0xA0000;
            biosmap->e820_map[biosmap->e820_entries].ends = biosmap->e820_map[index1+1].addr - 1;
            biosmap->e820_map[biosmap->e820_entries].size = biosmap->e820_map[biosmap->e820_entries].ends 
			                                                - biosmap->e820_map[biosmap->e820_entries].addr;
            biosmap->e820_map[biosmap->e820_entries].type = E820_TYPE_RESERVED;
            index1 = biosmap->e820_entries++;  // done get out early
        }
    }

    // Sort e820 memory by start address low to high
    sort_e820_map(biosmap);

    /* Look for overlap and try to fix it. At this point all the entries
     * are in order from low to high start address with region end address.
     * I filled in the start of video/rom area.
     */
    for(index1 = 0; index1 < biosmap->e820_entries-1; index1++) {
        if(biosmap->e820_map[index1].ends > biosmap->e820_map[index1+1].addr) {
            printf("overlap\n");
        }
    }
    return 0;
}

/*
 * Print out the current memory map for debug
 * 
 * Uses the crappy printf, but looks good enough.
 */
void print_biosmap(PHYMEMMAP *biosmap)
{
    // Debug --- print memory values
    uint8_t count = 0;

    printf("int12: %X int15-e801: %X\n", biosmap->int12_mem_k, 
                                                   biosmap->alt_mem_k);
    
    do {
        printf("e820: %0*X", 1, (biosmap->e820_map[count].addr>>32));
        printf("%0*X ", 8, biosmap->e820_map[count].addr);  
        printf("%0*X", 1, (biosmap->e820_map[count].size>>32));
        printf("%0*X ", 8, biosmap->e820_map[count].size);
        printf("%0*X", 1, (biosmap->e820_map[count].ends>>32));
        printf("%0*X ", 8, biosmap->e820_map[count].ends);
        printf(" %X\n", biosmap->e820_map[count].type);
        count++;   // inc number of entries
    } while (count < biosmap->e820_entries);

    return;
}

/* 
 * Sort the e820 map by start address
 * 
 * Nasty and quick bubble sort to get bios data in order, blow off
 * entry and overlap fields this early. Not very pretty, but I really expect
 * almost all raw bios memory to be in order, so no-harm-no-foul. I did test
 * by swapping 3-4 entries and every thing seemed to worked.
 * 
 * Is there a better way - probably, but it works and I don't give a shit
 * right now.
 */
static void sort_e820_map(mem_meminfo_t *biosmap)
{
    uint8_t  index1;
    uint8_t  index2;
    
    e820biosrec_t tmp_entry;    

    for(index1 = 0; index1 < biosmap->e820_entries-1; index1++) {
		for(index2 = 0; index2 < biosmap->e820_entries-index1-1; index2++) {
		    if(biosmap->e820_map[index2].addr > biosmap->e820_map[index2+1].addr) {
				tmp_entry.addr = biosmap->e820_map[index2].addr;
				tmp_entry.size = biosmap->e820_map[index2].size;
				tmp_entry.ends = biosmap->e820_map[index2].ends;
				tmp_entry.type = biosmap->e820_map[index2].type;
				biosmap->e820_map[index2].addr = biosmap->e820_map[index2+1].addr;
				biosmap->e820_map[index2].size = biosmap->e820_map[index2+1].size;
				biosmap->e820_map[index2].type = biosmap->e820_map[index2+1].type;
				biosmap->e820_map[index2].ends = biosmap->e820_map[index2+1].ends;
				biosmap->e820_map[index2+1].addr = tmp_entry.addr;
				biosmap->e820_map[index2+1].size = tmp_entry.size;
				biosmap->e820_map[index2+1].type = tmp_entry.type;
				biosmap->e820_map[index2+1].ends = tmp_entry.ends;
			}
		}
	}  // e820 addresses should be lowest to highest here

    return;
}

#pragma code_seg ( );


