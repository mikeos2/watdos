/****************************************************************************
 *
 *  memory.c -- Detect system memory.
 *
 *  ========================================================================
 *
 *    Version 2.0       Michael K Greene <mikeos2@gmail.com>
 *                      August 2019
 *
 *    Version 1.0       Michael K Greene <mike@mgreene.org>
 *                      July 2008
 *
 *  ========================================================================
 *
 *  Description: Pulled and modified from linux kernel source for use
 *               in os2ldr project. Modified it in Aug 2019 for watdos 
 *               project BIOS. This was originally a 16 bit detection
 *               routine, but I do this in watload. All the Int 15 88, 
 *               e801, and e820 results are store in a buffer and location 
 *               passed and stored at _MMPADDR.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************
 * 
 * General describtion:
 * This is a combination of a number of INT calls to get the lay of the 
 * land -- i.e. the system memory map. 
 * 
 * Int 15 e820
 * This call will be the primary source for the memory map. It provides
 * pretty much the entire layout and the type or use.
 * 
 * These other calls are just in case things do not work out as expected.
 * 
 * Int 12
 * This is an old call and returns the low memory 0-640
 * 
 * Int 15 88
 * This function may limit itself to reporting 15M (for legacy reasons) 
 * even if BIOS detects more memory than that. It may also report up to 
 * 64M. It only reports contiguous (usable) RAM.
 * 
 * Int 15 e801
 * It is built to handle the 15M memory hole, but stops at the next hole 
 * / memory mapped device / reserved area above that. That is, it is 
 * only designed to handle contiguous memory above 16M.
 * 
 * ax/cx = extended memory between 1M and 16M, in K (max 3C00h = 15MB)
 * bx/dx = extended memory above 16M, in 64K blocks
 * 
 * If the e820 call does not return a good map, a fake map can be 
 * generated using Int 12 + Int e801 results.
 * 
 * I guess you might find a memory hole, but that would be a very old 
 * machine, so I do not really care. I am gathering the extra data just
 * in case, but what is important - right now I am only using e820 results.
 * 
 * !!! Note - in Bochs above 1M is free RAM and an ACPI block. The e801
 * result returns the correct amount, but includes the ACPI block as free
 * RAM.
 */ 

/* -*- linux-c -*- ------------------------------------------------------- *
 *
 *   Copyright (C) 1991, 1992 Linus Torvalds
 *   Copyright 2007 rPath, Inc. - All Rights Reserved
 *
 *   This file is part of the Linux kernel, and is made available under
 *   the terms of the GNU General Public License version 2.
 *
 * ----------------------------------------------------------------------- */

/*
 * arch/i386/boot/memory.c
 *
 * Memory detection code
 */

/* Little linux blah-blah --- from old source file

   Determine system memory size

   Try three different memory detection schemes. -- MKG: Actual 4, I added
   an Int 12 call.

   First, try E820h, which lets us assemble a memory map, then try E801h, which
   returns a 32-bit memory size, and finally 88h, which returns 0-64 MB.

   Method E820H populates a table in the empty_zero_block that contains a list of
   usable address/size/type tuples. In "linux/arch/i386/kernel/setup.c", this
   information is transferred into the e820map, and in "linux/arch/i386/mm/init.c",
   that new information is used to mark pages reserved or not.

   Method E820H:
   Get the BIOS memory map. E820h returns memory classified into different types
   and allows memory holes. We scan through this memory map and build a list of the
   first 32 memory areas {up to 32 entries or BIOS says that there are no more entries},
   which we return at "E820MAP".
   [See URL: http://www.teleport.com/ acpi/acpihtml/topic245.htm]

   Method E801H:
   We store the 0xe801 memory size in a completely different place, because it will
   most likely be longer than 16 bits.

   This is the sum of 2 registers, normalized to 1 KB chunk sizes: %ecx = memory size
   from 1 MB to 16 MB range, in 1 KB chunks + %edx = memory size above 16 MB, in
   64 KB chunks.

   Ye Olde Traditional Methode:
   BIOS int. 0x15/AH=0x88 returns the memory size (up to 16 MB or 64 MB, depending
   on the BIOS). 

   #endif
*/

#include "memory.h"
#include "rmdata.h"

extern realmode_data RMTableData;                  // memory org stored by watload
extern struct bootparams boot_params;


extern int8_t sanitize_e820_map(mem_meminfo_t *);  // clean and construct full memmap
extern void print_biosmap(mem_meminfo_t *);        // routinue to display memmap

// e820_32.c
//extern int8_t sanitize_e820_map(struct e820entry *biosmap, uint16_t *pnr_map);
//extern int8_t copy_e820_map(struct e820entry *biosmap, uint16_t nr_map);
//extern void print_memory_map(char *who, struct e820entry *biosmap, uint16_t nr_map);

/* The new 2019 stuff:
 * 
 * We do the memory detection in watload while still in 16 bit with BIOS calls.
 * Everything is stuff in an area of memory and the offset is passed and stored 
 * at _MMPADDR (the area is low so offset only passed, no seg right now). It is
 * in the following format:
 * 
 * org _MMPADDR
 * <----   64 bit wide                                            E820                  ---->
 * Int15/88 |                     Int15/801                     | Count   |  Int12  | 0xAA55
 * 2 bytes  | ax-2 bytes | bx-2 bytes | cx-2 bytes | dx-2 bytes | 2 bytes | 2 bytes | 2 bytes
 * 
 * E820 memory entries follow 24 bytes each:
 *  
 * Chunk start  8 bytes                            |    Size  8 bytes                          |    
 * Type 4 bytes          |   Filler 90 90 90 90    |<-- end 24 byte field
 * 
 * ... e820 fields continue
 * 
 */

#pragma code_seg ( _TEXT );

/* detect_memory( )
 *
 * Detect all system memory and fills global structure SYSMEMORY.
 *
 * SYSMEMORYMAX == 32 -- see ldrinit.h
 */
uint8_t detect_memory( void )
{
    // loaction of BIOS memmap passed from watload
    uint16_t *pmem = (uint16_t *)RMTableData.MEMMapAddr;

    // meminfo will hold BIOS raw memmap retrieved from MEMMapAddr area
    mem_meminfo_t  meminfo; 
    
    // **** int 0x15 - 8800, old way to get extended memory
    // number of contiguous KiB of usable RAM starting at 0x00100000
    // 
    // Note: result will contain 0xAA55 is carry was set during INT
    meminfo.ext_mem_k = *pmem++;

    // **** int 0x15 - e801 results
    uint16_t ax88 = *pmem++;
    uint16_t bx88 = *pmem++;
    uint16_t cx88 = *pmem++;
    uint16_t dx88 = *pmem++;
    
    /* Do we really need to do this? -- linux source comment 
     * Reason:
     * There are some BIOSes that always return with AX = BX = 0. Use 
     * the CX/DX pair in that case. Some other BIOSes will return CX = DX = 0. 
     * Linux initializes the CX/DX pair to 0 before the INT opcode, and 
     * then uses CX/DX, unless they are still 0 (when it will use AX/BX)
     * 
     * Note: all four results will contain 0xAA55 is carry was set 
     * during INT
     */
    if (cx88 || dx88) {
        ax88 = cx88;
        bx88 = dx88;
    }
    
    /* ax88: extended memory between 1M and 16M, in K (max 3C00h = 15MB)
     * bx88: extended memory above 16M, in 64K blocks
     */

    // Check ax88 is not greater than 15 meg     
    if (ax88 > 15*1024) meminfo.alt_mem_k = 0xAA55;  /* Bogus! - skip */
    else {
        /* This ignores memory above 16MB if we have a memory hole
           there.  If someone actually finds a machine with a memory
           hole at 16MB and no support for 0E820h they should probably
           generate a fake e820 map. */
        meminfo.alt_mem_k = (ax88 == 15*1024) ? (bx88 << 6)+ax88 : ax88;
    }	
	
    // **** int 0x15 - e820 results
    meminfo.e820_entries  = *pmem++;     // number of e820 entries

    // the next field should be 0xAA55 -- I stuck int12 results in at a
    // later date which remove a dummy field
//    pmem+=2;
   
    meminfo.int12_mem_k  = *pmem++;     // number of e820 entries
    
    // set to first e820 entry in _MMPADDR area
    uint64_t *entry_addr = (uint64_t *)(RMTableData.MEMMapAddr+0x10);

    uint16_t count = 0;

    do {
		meminfo.e820_map[count].entry = count;
        meminfo.e820_map[count].addr = *entry_addr++;
        meminfo.e820_map[count].size = *entry_addr++;
        meminfo.e820_map[count].type = *entry_addr++;        
        count++;   // inc number of entries
    } while (count < meminfo.e820_entries);   
    
    // **** Memory info loaded - _MMPADDR area no longer valid **** //      
    /* Below this point clean up and add memmap to boot_params  */

    uint8_t result = sanitize_e820_map(&meminfo);
    
    // for future - if e820 is bad just fake the memory map:
    // low mem: biosmap->int12_mem_k  hign mem: biosmap->alt_mem_k

    count = 0;
    
    boot_params.PhyMemMap.int12_mem_k  = meminfo.int12_mem_k;
    boot_params.PhyMemMap.ext_mem_k    = meminfo.ext_mem_k; 
    boot_params.PhyMemMap.alt_mem_k    = meminfo.alt_mem_k;
    boot_params.PhyMemMap.e820_entries = meminfo.e820_entries;

    do {
        boot_params.PhyMemMap.e820_map[count].entry = meminfo.e820_map[count].entry;
        boot_params.PhyMemMap.e820_map[count].addr  = meminfo.e820_map[count].addr;
        boot_params.PhyMemMap.e820_map[count].size  = meminfo.e820_map[count].size;
        boot_params.PhyMemMap.e820_map[count].ends  = meminfo.e820_map[count].ends;
        boot_params.PhyMemMap.e820_map[count].type  = meminfo.e820_map[count].type;
        count++;
    } while (count < boot_params.PhyMemMap.e820_entries);

    
    //print_biosmap(&meminfo);

    return result;
}
#pragma code_seg ( );
