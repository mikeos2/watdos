/****************************************************************************
 *
 *  idt.c -- IDT routines and variables
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Interrupt Descriptor Table
 * 
 *  Ref: http://www.brokenthorn.com/Resources/
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#include <stdinit.h>
#include "idt.h"
#include "printf.h"
#include "tty.h"


static struct idt_ptr_struct  IDTTable = {0,0};


/* Initialize IDT values passed in from loader
 * size and linear location
 */
void idt_init(uint16_t limit, uint32_t base)
{
    IDTTable.limit = limit;
    IDTTable.base = base;

    return;
}

uint32_t IDT_get_base( void)
{
    return IDTTable.base;
}


/* Return current IDT values 
 * size and linear location
 */
void IDT_get(idt_ptr_t *IDT_Table)
{
    IDT_Table->limit = IDTTable.limit;
    IDT_Table->base  = IDTTable.base;

    return;
}

//! default handler to catch unhandled system interrupts.
void default_except_handler () {
    early_gotoxy(0,0);
    early_clearscreen( );
    early_setcolor(COLOR_RED, COLOR_WHITE);
    printf("*** i86_default_handler: Unhandled Exception");
 
    for(;;);
}




