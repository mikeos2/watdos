/****************************************************************************
 *
 *  tty.h -- Send charater to screen plus helper routines and serial output.
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      May 2019
 *
 *  ========================================================================
 *
 *  Description: Header contains simple screen and comm routines for early 
 *               debug use after entering 32 bit pmode and before IDT is setup
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
 
#ifndef TTY_H
#define TTY_H

#include <stdint.h>

/*
 *        Attribute               |       Character
 *    7  | 6  5  4  | 3  2  1  0  |  7  6  5  4  3  2  1  0
 * Blink | BG color | X  FG color |       Code point
 *   or             |- bright bit
 * Bright
 *
 * Each screen character is actually represented by two bytes aligned as a 
 * 16-bit word accessible -- bottom line, store litle endian: manual store
 * char byte -- color byte
 *
 * The default display colours set by the BIOS upon booting are 0x0F: 0 (black) 
 * for the background and 7 (White) + 8 (Bright) for the foreground.
 *
 * In text mode 0, the following standard colour palette is available for use. 
 * You can change this palette with VGA commands.
 *                Num +    
 * Num Name       bright bit  Name
 * 0   Black      0+8=8       Dark Gray
 * 1   Blue       1+8=9       Light Blue
 * 2   Green      2+8=A       Light Green
 * 3   Cyan       3+8=B       Light Cyan
 * 4   Red        4+8=C       Light Red
 * 5   Magenta    5+8=D       Light Magenta
 * 6   Brown      6+8=E       Yellow
 * 7   Light Gray 7+8=F       White
 */

#define COLOR_BLACK        0x00
#define COLOR_BLUE         0x01
#define COLOR_GREEN        0x02
#define COLOR_CYAN         0x03
#define COLOR_RED          0x04
#define COLOR_MAGENTA      0x05
#define COLOR_BROWN        0x06
#define COLOR_LTGRAY       0x07
#define COLOR_DKGRAY       0x08
#define COLOR_LTBLUE       0x09
#define COLOR_LTGREEN      0x0A
#define COLOR_LTCYAN       0x0B
#define COLOR_LTRED        0x0C
#define COLOR_LTMAGENTA    0x0D
#define COLOR_YELLOW       0x0E
#define COLOR_WHITE        0x0F

struct ScreenStatus {
    unsigned int _xPos;
    unsigned int _yPos;
    unsigned _startX;
    unsigned _startY;
    unsigned _scrnwidth;
    unsigned _color;
};

#ifdef PUTSIN           // keep from stepping on OW headers
void puts(const char *);
void putchar(int);
#endif

void early_clearscreen(void);
void early_setcolor(uint8_t backcolor, uint8_t forecolor);
void early_gotoxy(unsigned x, unsigned y); 
int getxpos(void);
int getypos(void);


//int getchar(void);
//void kbd_flush(void);
//int getchar_timeout(void);

#endif /* TTY_H */
