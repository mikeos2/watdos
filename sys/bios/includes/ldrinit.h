/****************************************************************************
 *
 *  ldrinit.h -- loader toplevel param structure
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mike@mgreene.org>
 *                      June 2008
 *
 *  ========================================================================
 *
 *  Description: Structure of passed from the asm loader code to the C
 *               loader code.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/


#ifndef LDRINIT_H
#define LDRINIT_H


#pragma pack ( push, 1)


typedef struct _ENTRYINFO {
    uint16_t   bootflags;               // entry value bootflags (dh)
    uint16_t   bpbptroff;               // entry value BPB offset
    uint16_t   bpbptrseg;               // entry value BPB segment
    uint16_t   ftabptroff;              // entry value Filetable offset
    uint16_t   ftabptrseg;              // entry value Filetable segment
    uint16_t   endos2ldr;               // end of os2ldr after load
    uint16_t   endoftext;               // end of text segment
} ENTRYINFO, *PENTRYINFO;


typedef struct _PORTPARAMS {
    uint16_t   comport1;
    uint16_t   comport2;
    uint16_t   comport3;
    uint16_t   comport4;
    uint16_t   lptport1;
    uint16_t   lptport2;
    uint16_t   lptport3;
} PORTPARAMS, *PPORTPARAMS;

typedef struct _CPUINFO {
    uint16_t   cpu_level;
    uint16_t   cpu_model;
    uint16_t   cpu_fpu;                 // true has fpu
    uint32_t   max_funcnum;             // CPUID Func 0x00000000 - eax return
    uint32_t   cpuid_info1;             // CPUID Func 0x00000001 - eax return
    uint32_t   cpuid_info2;             // CPUID Func 0x00000001 - edx return
    uint32_t   cpuid_info3;             // CPUID Func 0x00000001 - ecx return
    uint32_t   cpuid_info4;             // CPUID Func 0x80000001 - edx return
    uint32_t   cpuid_info5;             // CPUID Func 0x80000001 - ecx return
    uint8_t    cpu_vendor[12];          // CPUID Func 0x00000000 - vendor return
} CPUINFO, *PCPUINFO;


typedef struct _CINFOSTRUC {
    uint16_t   port;
    uint16_t   addr;
} CINFOSTRUC, *PCINFOSTRUC;


// misc system config info - see sysconfig.c
typedef struct _SYSINFO {
    uint16_t   syscfg1;
    uint16_t   syscfg2;
    uint16_t   syscfg3;
    uint16_t   syscfg4;
} SYSINFO, *PSYSINFO;


typedef struct _PCIINFO {
    uint32_t   pmodephyaddr;            // physical address of protected-mode entry point
    uint8_t    hwcharactristics;        // PCI hardware characteristics
    uint8_t    interfacelvlmaj;         // PCI interface level major version (BCD)
    uint8_t    interfacelvlmin;         // PCI interface level minor version (BCD)
    uint8_t    lastpcibus;              // number of PCI bus in system
    uint16_t   probestatus;
} PCIINFO, *PPCIINFO;


// Following structure found in ddk doshlp.inc and was called
// PHYSMEMBLOCK. I will use this as my global memory map
/*
#define SYSMEMORYMAX    32

typedef struct _SYSMEMORY {
    uint32_t   memstartaddr;            // final start address of mem chunk
    uint32_t   memsize;                 // size of chunk
    uint32_t   whichcpulocal;           // I have no idea what this is for
    uint32_t   whichcpucached;          // I have no idea what this is for
} SYSMEMORY, *PSYSMEMORY;
*/

// This needs to be duplicated in main.asm
typedef struct _LOADERDATA {
    uint16_t      *memory;     // ? pointer to memory layout here ?
    uint16_t      *bootbpb;    // BPB loction here
    uint8_t       bootdrive;
    uint8_t       bootflags;
    uint16_t      dhsegment;   // includes doshlps and oemhlp
    uint16_t      dhsize;      // should just be offset since 0 based
    uint16_t _far *dhlocation; // location of doshlp table
    uint8_t       unknown1;
    uint8_t       unknown2;
    uint16_t      oemoff;      // oemhlp strategy
} LOADERDATA, *PLOADERDATA;



#pragma pack ( pop )

#endif  /* LDRINIT_H */

