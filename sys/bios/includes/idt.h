/****************************************************************************
 *
 *  idt.h -- IDT defines
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Interrupt Descriptor Table
 * 
 *  Ref: http://www.brokenthorn.com/Resources/
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef IDTDEF_H
#define IDTDEF_H

#include <stdint.h>


#define I86_MAX_INTERRUPTS		256

/* IDT Flags defines 8 bit [00000000] 
 * 
 *  7                           0
 * +---+---+---+---+---+---+---+---+
 * | P |  DPL  | S |    GateType   |
 * +---+---+---+---+---+---+---+---+
 * 
 */
#define IDT_FLAG_PRESENT                      0x80       // Set to 1 for used interrupts
#define IDT_FLAG_NOT_PRESENT                  0x00       // Set to 0 for unused interrupts

#define IDT_FLAG_STORESEG_OFF                 0x00       // Set to 0 for interrupt and trap gates
#define IDT_FLAG_STORESEG_ON                  0x10

#define IDT_FLAG_DPL_RING0                    0x00       // Gate call protection. Specifies which 
#define IDT_FLAG_DPL_RING1                    0x20       // privilege Level the calling Descriptor minimum
#define IDT_FLAG_DPL_RING2                    0x40
#define IDT_FLAG_DPL_RING3                    0x60

#define IDT_FLAG_TASKGATE_TASKGATE32          0x05      // 80386 32 bit task gate
#define IDT_FLAG_TASKGATE_INTERRUPTGATE16     0x06      // 80286 16-bit interrupt gate  
#define IDT_FLAG_TASKGATE_TTRAPGATE16         0x07      // 80286 16-bit trap gate
#define IDT_FLAG_TASKGATE_INTERRUPTGATE32     0x0E      // 80386 32-bit interrupt gate
#define IDT_FLAG_TASKGATE_TTRAPGATE32         0x0F      // 80386 32-bit trap gate


struct idt_ptr_struct {
    uint16_t limit;            // size of IDT
    uint32_t base;             // address start of IDT (INT 0)
};
typedef struct idt_ptr_struct idt_ptr_t;

// Interrupt / Trap Gate
#pragma pack (push, 1)
struct idt_descriptor {
    uint16_t    OffsetLo;      // bits 0-16 address of the 32 bit entry point of the ISR
	uint16_t    Selector;      // code selector in gdt, selector of the interrupt function
	uint8_t	    Reserved;      // reserved, shold be 0
	uint8_t     Flags;         // bit flags, defined above
	uint16_t    OffsetHi;      // bits 16-32 address of the 32 bit entry point of the ISR
};
#pragma pack (pop)
typedef struct idt_descriptor idt_intr_desc_t;
typedef struct idt_descriptor idt_trap_desc_t;
typedef struct idt_descriptor idt_task_desc_t;

void idt_init(uint16_t limit, uint32_t base);
uint32_t IDT_get_base( );

#endif /* IDTDEF_H */
