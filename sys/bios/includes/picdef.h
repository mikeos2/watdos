/****************************************************************************
 *
 *  picdef.h -- PIC defines
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: 8259 Programmable Interrupt Controller parameters
 * 
 *  For setting up and relocating IRQs to avoid hardware exceptions
 *  IRQs 0-31
 * 
 *  Ref: http://www.brokenthorn.com/Resources/OSDevPic.html
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef PICDEF_H
#define PICDEF_H

/*
 Chip - Purpose         I/O port
 Master PIC - Command   0x0020
 Master PIC - Data      0x0021
 Slave PIC - Command    0x00A0
 Slave PIC - Data       0x00A1  
*/
#define PIC1_PORT0       0x20              // Master PIC
#define PIC1_PORT1       (PIC1_PORT0 + 1)
#define PIC2_PORT0       0xA0              // Slave PIC
#define PIC2_PORT1       (PIC2_PORT0 + 1)

/* Definitions */
#define NUMBER_PICS      2               // x86 2 PICs
#define NUM_IRQ_PER_PIC  8
#define NUM_IRQ          (NUMBER_PICS * NUM_IRQ_PER_PIC)

#define PIC1             0x00
#define PIC2             0x01

#define PIC1_COMMAND     PIC1_PORT0
#define PIC1_DATA        PIC1_PORT1
#define PIC1_STATUS      PIC1_PORT0
#define PIC1_IMR         PIC1_PORT1

#define PIC2_COMMAND     PIC2_PORT0
#define PIC2_DATA        PIC2_PORT1
#define PIC2_STATUS      PIC2_PORT0
#define PIC2_IMR         PIC2_PORT1

/*
 IRQ Lines for ICW 2 (Primary PIC)
 Bit Number	IRQ Line
  0         IR0
  1         IR1
  2         IR2
  3         IR3
  4         IR4
  5         IR5
  6         IR6
  7         IR7
*/

#define PIC_SLAVE_IRQ    2               // Cascade for 8259A Slave

/*    
x86 Hardware Interrupts
8259A Input pin	Interrupt Number	Description
IRQ0               0x08                Non-maskable Interrupt (NMI); system timer
IRQ1               0x09                Keyboard
IRQ2               0x0A                Cascade for 8259A Slave controller
IRQ3               0x0B                Serial port 2
IRQ4               0x0C                Serial port 1
IRQ5               0x0D                Parallel Port 2 / Sound card
IRQ6               0x0E                Diskette drive
IRQ7               0x0F                Parallel Port 1
IRQ8/IRQ0          0x70                CMOS Real time clock
IRQ9/IRQ1          0x71                CGA vertical retrace
IRQ10/IRQ2         0x72                Reserved - Network interface; USB host controller
IRQ11/IRQ3         0x73                Reserved - Video adapter; SCSI host adapter
IRQ12/IRQ4         0x74                Reserved - PS/2 mouse port
IRQ13/IRQ5         0x75                FPU
IRQ14/IRQ6         0x76                Hard disk controller
IRQ15/IRQ7         0x77                Reserved
*/
// The following devices use PIC 1 to generate interrupts
#define		I86_PIC_IRQ_TIMER           0
#define		I86_PIC_IRQ_KEYBOARD        1
#define		I86_PIC_IRQ_CASCADE         2   // Cascade for 8259A Slave
#define		I86_PIC_IRQ_SERIAL2         3
#define		I86_PIC_IRQ_SERIAL1         4
#define		I86_PIC_IRQ_PARALLEL2       5
#define		I86_PIC_IRQ_DISKETTE        6
#define		I86_PIC_IRQ_PARALLEL1       7
 
// The following devices use PIC 2 to generate interrupts
#define		I86_PIC_IRQ_CMOSTIMER       0   
#define		I86_PIC_IRQ_CGARETRACE      1
#define		I86_PIC_IRQ_AUXILIARY1      2
#define		I86_PIC_IRQ_AUXILIARY2      3
#define		I86_PIC_IRQ_AUXILIARY3      4
#define		I86_PIC_IRQ_FPU             5
#define		I86_PIC_IRQ_HDC1            6   // ATA channel 1
#define		I86_PIC_IRQ_HDC2            7   // ATA channel 1

// Initialization Control Word (ICW) 1
#define ICW1_ICW4_NEEDED            0x01    // IC4	If set(1), the PIC expects to recieve IC4 during initialization.
#define ICW1_NO_ICW4_NEEDED         0x00    //      If cleared, no IC4
#define ICW1_SINGLE                 0x02    // SNGL	If set(1), only one PIC in system. 
#define ICW1_CASCADE                0x00    //      If cleared, PIC is cascaded with slave PICs, and ICW3 must be sent to controller.
#define ICW1_INTERVAL4              0x04    // ADI	If set (1), CALL address interval is 4, else 8. 
#define ICW1_INTERVAL8              0x00    //      This is useually ignored by x86, and is default to 0
#define ICW1_LEVEL_TRIG             0x08    // LTIM	If set (1), Operate in Level Triggered Mode. 
#define ICW1_EDGE_TRIG              0x00    //      If Not set (0), Operate in Edge Triggered Mode
#define ICW1_INIT                   0x10    // INIT Initialization bit. Set 1 if PIC is to be initialized
#define ICW1_NO_INIT                0x00    //      If cleared, no initialization
#define ICW1_MCS_1                  0x00    // MCS-80/85: Interrupt Vector Address. x86 Architecture: Must be 0
#define ICW1_MCS_2                  0x00    // MCS-80/85: Interrupt Vector Address. x86 Architecture: Must be 0
#define ICW1_MCS_3                  0x00    // MCS-80/85: Interrupt Vector Address. x86 Architecture: Must be 0

// Initialization Control Word (ICW) 2
/*
 * If an ICW1 was sent to the PICs (With the initialization bit set), you must send ICW2 next.
 * 
 * Bit Number    Value       Description
 * 0-2           A8/A9/A10   Address bits A8-A10 for IVT when in MCS-80/85 mode.
 * 3-7           A11(T3)/    Address bits A11-A15 for IVT when in MCS-80/85 mode. 
 *               A12(T4)/        In 80x86 mode, specifies the interrupt vector address. May be set to 0 in x86 mode.
 *               A13(T5)/
 *               A14(T6)/
 *               A15(T7)	
 */
// PMode relocate IRQs out of hardware exception range 
#define PIC1_BASE                   0x20            // Start at 32
#define PIC2_BASE                   0x28            // Start at 40

#define ICW4_8086_MODE              0x01
#define ICW4_8085_MODE              0x00
#define ICW4_AUTO_EOI               0x02
#define ICW4_NORM_EOI               0x00
#define ICW4_NON_BUF_MODE           0x00
#define ICW4_BUF_MODE_SLAVE         0x08
#define ICW4_BUF_MODE_MASTER        0x0C
#define ICW4_SPEC_FULLY_NESTED      0x10
#define ICW4_NOT_SPEC_FULLY_NESTED  0x00


// ******* Operation Command Words (OCW) ********

// OCW 1 represents the value inside of the Interrupt Mask register (IMR). 
/* To abtain the current OCW 1, all you need to do is read from the IMR.

Example:
in	al, 0x21		; read in the primary PIC Interrupt Mask Register (IMR)
and	al, 0xEF		; 0xEF => 11101111b. This sets the IRQ4 bit (Bit 5) in AL
out	0x21, al		; write the value back into IMR
*/

/*
Operation Command Word (OCW) 2
Bit Number  Value      Description
0-2         L0/L1/L2   Interrupt level upon which the controller must react
3-4         0          Reserved, must be 0
5           EOI        End of Interrupt (EOI) request
6           SL         Selection
7           R          Rotation option
*/

// OCW2 bit masks (L0/L1/L2) Use when sending commands
#define		I86_PIC_OCW2_MASK_L1		0x01		// 00000001	 Level 1 interrupt level
#define		I86_PIC_OCW2_MASK_L2		0x02		// 00000010	 Level 2 interrupt level
#define		I86_PIC_OCW2_MASK_L3		0x04		// 00000100	 Level 3 interrupt level

// OCW2 Commands
#define		I86_PIC_OCW2_MASK_EOI		0x20		// 00100000	 End of Interrupt command
#define		I86_PIC_OCW2_MASK_SL		0x40		// 01000000	 Select command
#define		I86_PIC_OCW2_MASK_ROTATE	0x80		// 10000000  Rotation command

/*
R   SL  EOI Description
0   0   0   Rotate in Automatic EOI mode (CLEAR)
0   0   1   Non specific EOI command
0   1   0   No operation
0   1   1   Specific EOI command
1   0   0   Rotate in Automatic EOI mode (SET)
1   0   1   Rotate on non specific EOI
1   1   0   Set priority command
1   1   1   Rotate on specific EOI
*/

/* Output Control Words for the 8259 PIC */
#define OCW2_ROTATE_AUTO_CLEAR_EOI      0x00
#define OCW2_NON_SPECIFIC_EOI           I86_PIC_OCW2_MASK_EOI
#define OCW2_NO_OPERATION               I86_PIC_OCW2_MASK_SL
#define OCW2_SPECIFIC_EOI               (I86_PIC_OCW2_MASK_EOI | I86_PIC_OCW2_MASK_SL)
#define OCW2_ROTATE_AUTO_SET_EOI        I86_PIC_OCW2_MASK_ROTATE        
#define OCW2_ROTATE_NON_SPECIFIC_EOI    (I86_PIC_OCW2_MASK_EOI | I86_PIC_OCW2_MASK_ROTATE)
#define OCW2_SET_PRIORITY               (I86_PIC_OCW2_MASK_SL | I86_PIC_OCW2_MASK_ROTATE)
#define OCW2_ROTATE_SPECIFIC_EOI        (I86_PIC_OCW2_MASK_EOI | I86_PIC_OCW2_MASK_SL | I86_PIC_OCW2_MASK_ROTATE)

// OCW3 Special Massk Mode
#define OCW3_READ_IRR                0x0A
#define OCW3_READ_ISR                0x0B

/* 
 * IVT Exception IRQ Numbers in PMode 
 * after PIC relocation
 */
#define MAX_IRQ          48

#define IRQ0   0x00   // Divide Error
#define IRQ1   0x01   // Debug
#define IRQ2   0x02   // NMI Interrupt
#define IRQ3   0x03   // Breakpoint
#define IRQ4   0x04   // Overflow
#define IRQ5   0x05   // BOUND Range Exceeded
#define IRQ6   0x06   // Invalid Opcode (UnDefined Opcode)
#define IRQ7   0x07   // Device Not Available (No Math Coprocessor)
#define IRQ8   0x08   // Double Fault
#define IRQ9   0x09   // CoProcessor Segment Overrun
#define IRQA   0x0A   // Invalid TSS
#define IRQB   0x0B   // Segment Not Present
#define IRQC   0x0C   // Stack Segment Fault
#define IRQD   0x0D   // General Protection
#define IRQE   0x0E   // Page Fault
#define IRQF   0x0F   // Intel reserved
#define IRQ10  0x10   // Floating-Point Error (Math Fault)
#define IRQ11  0x11   // Alignment Check
#define IRQ12  0x12   // Machine Check
#define IRQ13  0x13   // Streaming SIMD Extensions
#define IRQ14  0x14   // Intel reserved
#define IRQ15  0x15   // Intel reserved
#define IRQ16  0x16   // Intel reserved
#define IRQ17  0x17   // Intel reserved
#define IRQ18  0x18   // Intel reserved
#define IRQ19  0x19   // Intel reserved
#define IRQ1A  0x1A   // Intel reserved
#define IRQ1B  0x1B   // Intel reserved
#define IRQ1C  0x1C   // Intel reserved
#define IRQ1D  0x1D   // Intel reserved
#define IRQ1E  0x1E   // Intel reserved
#define IRQ1F  0x1F   // Intel reserved
#define IRQ20  0x20   // Non-maskable Interrupt (NMI); system timer
#define IRQ21  0x21   // Keyboard
#define IRQ22  0x22   // Cascade for 8259A Slave controller
#define IRQ23  0x23   // Serial port 2
#define IRQ24  0x24   // Serial port 1
#define IRQ25  0x25   // Parallel Port 2 / Sound card
#define IRQ26  0x26   // Diskette drive
#define IRQ27  0x27   // Parallel Port 1
#define IRQ28  0x28   // CMOS Real time clock
#define IRQ29  0x29   // CGA vertical retrace
#define IRQ2A  0x2A   // Reserved - Network interface; USB host controller
#define IRQ2B  0x2B   // Reserved - Video adapter; SCSI host adapter
#define IRQ2C  0x2C   // Reserved - PS/2 mouse port
#define IRQ2D  0x2D   // FPU
#define IRQ2E  0x2E   // Hard disk controller
#define IRQ2F  0x2F   // Hard disk controller

#endif /* PICDEF_H */
