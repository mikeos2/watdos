/****************************************************************************
 *
 *  string.h -- String routines.
 *
 *  ========================================================================
 *
 *    Version 1.1       Michael K Greene <mikeos2@gmail.com>
 *                      May 2019
 * 
 *  ========================================================================
 *
 *  Description: Pulled and modified from linux kernel source and some
 *               based on http://www.brokenthorn.com code.
 *
 *               Simple string routines for early debug use
 *               after entering 32 bit pmode and before IDT is setup
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
 
#ifndef STRING_H
#define STRING_H

 #ifndef _SIZE_T_DEFINED
 #define _SIZE_T_DEFINED
  #define _SIZE_T_DEFINED_
  typedef unsigned size_t;
  typedef size_t   __w_size_t;
 #endif


extern void      *memchr( const void *__s, int __c, size_t __n );
extern int       memcmp( const void *__s1, const void *__s2, size_t __n );
extern void      *memcpy( void *__s1, const void *__s2, size_t __n );
extern void      *memmove( void *__s1, const void *__s2, size_t __n );
extern void      *memset( void *__s, int __c, size_t __n );

int strcmp(const char *str1, const char *str2);
size_t strnlen(const char *s, size_t maxlen);
unsigned int atou(const char *s);

#endif /* STRING_H */
