/****************************************************************************
 *
 *  bootparams.h -- structure boot paramaters
 *
 *  ========================================================================
 *
 *    Version 2.0       Michael K Greene <mikeos2@gmail.com>
 *                      October 2019 
 *
 *  ========================================================================
 *
 *  Description: Master structure for data gathered at boot.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
 
#ifndef __BOOTPARAMS_H
#define __BOOTPARAMS_H

#include <stdint.h>


// *********************************************************************
// Memory structures
// *********************************************************************

#define E820MAP	  0x2d0       /* our map */
#define E820MAX	  50          // be a little more conservative

/*
 * The E820 memory region entry of the boot protocol ABI:
 */
#pragma pack ( push, 1)
struct e820entry {
	uint8_t   entry;       /* entry number            */
    uint64_t  addr;        /* start of memory segment */
    uint64_t  size;        /* size of memory segment  */
    uint64_t  ends;        /* end of memory segment   */    
    uint32_t  type;        /* type of memory segment  */
};
#pragma pack ( pop )
typedef struct e820entry e820entry_t;

// Physical memory layout after fixing
typedef struct PHYMEMMAP {
    uint16_t    int12_mem_k;
    uint16_t    ext_mem_k;
    uint16_t    alt_mem_k;
    uint16_t    e820_entries;
    e820entry_t e820_map[E820MAX];
} PHYMEMMAP;

// *********************************************************************
// Global Data Structure
// *********************************************************************
/* like Linux "zeropage" */

#pragma pack ( push, 1)
struct bootparams {
	PHYMEMMAP PhyMemMap;                  // master physical memory map of system
};
#pragma pack ( pop )


#endif /* __BOOTPARAMS_H */
