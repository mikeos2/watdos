/****************************************************************************
 *
 *  ctype.h -- isdigit and isxdigit.
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      May 2019
 *
 *  ========================================================================
 *
 *  Description: Early ctype defines 
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
/*
 * WatDos -- tty.c
 *
 * Modified for use with Open Watcom - M Greene May 2019
 * 
 * arch/x86/boot/ctype.h - newer version of the kernel (5.1.X)
 */ 

/* SPDX-License-Identifier: GPL-2.0 */
#ifndef BOOT_CTYPE_H
#define BOOT_CTYPE_H

static inline int isdigit(int ch)
{
	return (ch >= '0') && (ch <= '9');
}

static inline int isxdigit(int ch)
{
	if (isdigit(ch))
		return 1; // true

	if ((ch >= 'a') && (ch <= 'f'))
		return 1; // true

	return (ch >= 'A') && (ch <= 'F');
}

#endif
