/****************************************************************************
 *
 *  portio.h -- low-level port input and output.
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: functions is used to do low-level port input and output
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/
 
#ifndef PORTIO_H
#define PORTIO_H

#include <stdint.h>

/*
 * My answer to the linux boot.h cpu_relax() which is a rep;nop;
 * delay, just inline a pause for the serial output.
 */
static void cpu_relax(void);
#pragma aux cpu_relax = "pause";

/* Basic port I/O */
static inline void outb(uint8_t v, uint16_t port)
{
    _asm {
        mov    al, v
        mov    dx, port
        out    dx, al
    }
}

static inline uint8_t inb(uint16_t port)
{
    uint8_t v = 0;

    _asm {
        mov    dx, port
        in     al, dx
        mov    v, al
    }
    return v;
}

static inline void outw(uint16_t v, uint16_t port)
{
    _asm {
        mov    ax, v
        mov    dx, port
        out    dx, ax
    }
}

static inline uint16_t inw(uint16_t port)
{
    uint16_t v = 0;

    _asm {
        mov    dx, port
        in     ax, dx
        mov    v, ax
    }
    return v;
}

static inline void outl(uint32_t v, uint16_t port)
{
    _asm {
        mov    eax, v
        mov    dx, port
        out    dx, eax
    }
}

static inline uint32_t inl(uint32_t port)
{
    uint32_t v = 0;

    uint16_t porttmp = (uint16_t)port;

    _asm {
        mov    dx, porttmp
        in     eax, dx
        mov    v, eax
    }
    return v;
}

#endif /* PORTIO_H */
