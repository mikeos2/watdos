/****************************************************************************
 *
 *  memory.h -- memory defines
 *
 *  ========================================================================
 *
 *    Version 2.0       Michael K Greene <mikeos2@gmail.com>
 *                      August 2019 
 *
 *    Version 1.0       Michael K Greene <mike@mgreene.org>
 *                      June 2008
 *
 *  ========================================================================
 *
 *  Description: Defines for memory routines.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef __MEMORY_H
#define __MEMORY_H

#include <stdint.h>

// Linux e820.h header

#include "bootparams.h"

/*
 * Legacy E820 BIOS limits us to 128 (E820MAX) nodes due to the
 * constrained space in the zeropage.  If we have more nodes than
 * that, and if we've booted off EFI firmware, then the EFI tables
 * passed us from the EFI firmware can list more nodes.  Size our
 * internal memory map tables to have room for these additional
 * nodes, based on up to three entries per node for which the
 * kernel was built: MAX_NUMNODES == (1 << CONFIG_NODES_SHIFT),
 * plus E820MAX, allowing space for the possible duplicate E820
 * entries that might need room in the same arrays, prior to the
 * call to sanitize_e820_map() to remove duplicates.  The allowance
 * of three memory map entries per node is "enough" entries for
 * the initial hardware platform motivating this mechanism to make
 * use of additional EFI map entries.  Future platforms may want
 * to allow more than three entries per node or otherwise refine
 * this size.
 */

#define E820NR	0x1e8       /* # entries in E820MAP */
 
#define E820_TYPE_RAM        1
#define E820_TYPE_RESERVED   2
#define E820_TYPE_ACPI       3
#define E820_TYPE_NVS        4
#define E820_TYPE_UNUSABLE   5
#define E820_TYPE_PMEM       7
#define E820_TYPE_PRAM       12

#define E820_RAM	1
#define E820_RESERVED	2
#define E820_ACPI	3
#define E820_NVS	4
#define E820_UNUSABLE	5
#define E820_PMEM	7

// following left in for ref --- not used by me

/*
 * This is a non-standardized way to represent ADR or NVDIMM regions that
 * persist over a reboot.  The kernel will ignore their special capabilities
 * unless the CONFIG_X86_PMEM_LEGACY option is set.
 *
 * ( Note that older platforms also used 6 for the same type of memory,
 *   but newer versions switched to 12 as 6 was assigned differently.  Some
 *   time they will learn... )
 */
#define E820_PRAM	12

/*
 * reserved RAM used by kernel itself
 * if CONFIG_INTEL_TXT is enabled, memory of this type will be
 * included in the S3 integrity calculation and so should not include
 * any memory that BIOS might alter over the S3 transition
 */
#define E820_RESERVED_KERN        128

// end not used

#define HIGH_MEMORY	(1024*1024)

#pragma pack ( push, 1)
struct e820biosrec {
	uint8_t   entry;       /* entry number            */
    uint64_t  addr;        /* start of memory segment */
    uint64_t  size;        /* size of memory segment  */
    uint64_t  ends;        /* end of memory segment   */    
    uint32_t  type;        /* type of memory segment  */
};
#pragma pack ( pop )
typedef struct e820biosrec e820biosrec_t;


#pragma pack ( push, 1)
typedef struct MEMINFO {
	uint16_t    int12_mem_k;
    uint32_t    ext_mem_k;
    uint32_t    alt_mem_k;
    uint16_t    e820_entries;
    e820biosrec_t e820_map[E820MAX];  // using 128 as default MKG 100819
};
#pragma pack ( pop )
typedef struct MEMINFO mem_meminfo_t;

// can this go?
#pragma pack ( push, 1)
struct e820map {
    uint32_t  nr_map;
    struct e820entry map[E820MAX];
};
#pragma pack ( pop )
typedef struct e820map mem_e820map_t;
// end go

// Following structure found in ddk doshlp.inc and was called
// PHYSMEMBLOCK. I will use this as my global memory map
#define SYSMEMORYMAX    32

typedef struct _SYSMEMORY {
    uint32_t   memstartaddr;            // final start address of mem chunk
    uint32_t   memsize;                 // size of chunk
    uint32_t   whichcpulocal;           // I have no idea what this is for
    uint32_t   whichcpucached;          // I have no idea what this is for
} SYSMEMORY, *PSYSMEMORY;


#define ISA_START_ADDRESS	0xa0000
#define ISA_END_ADDRESS		0x100000

#define BIOS_BEGIN		0x000a0000
#define BIOS_END		0x00100000

#define BIOS_ROM_BASE		0xffe00000
#define BIOS_ROM_END		0xffffffff

uint8_t detect_memory( void );

#endif /* __MEMORY_H */

