/****************************************************************************
 *
 *  pics.h -- 8259 PIC routines
 *
 *  ========================================================================
 *
 *    Version 2.0       Michael K Greene <mikeos2@gmail.com>
 *                      November 2019 
 *
 *  ========================================================================
 *
 *  Description: Master structure for data gathered at boot.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef __PICS_H
#define __PICS_H

#include <stdint.h>

void init_pics( void);

uint8_t pic_status(uint8_t picNum);
uint8_t pic_imr(uint8_t picNum);

#endif  // __PICS_H
