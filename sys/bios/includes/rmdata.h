/****************************************************************************
 *
 *  gdt.h -- GDT defines
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Global Descriptor Table
 * 
 *  Ref: http://www.brokenthorn.com/Resources/
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef RMDATA_H
#define RMDATA_H

#include <stdint.h>

/* NOTE ********
 * rmdata.inc must be kept in sync with this header until I can get a 
 * workable solution such as h2inc to automatically keep in sync.
 */

#pragma pack (push, 2)
typedef struct realmode_data {
    uint16_t    IDTAddr;                     // IDT location      
    uint16_t    GDTAddr;                     // GDT location  
    uint16_t    MEMMapAddr;                  // BIOS memmap location
    uint16_t    INT11RET;                    // Return from INT 0x11
    uint16_t    INT15C0Size;                 // INT15 C0 number of bytes following
    uint8_t     INT15C0Data[0x0F];           // Actual returned data from INT15 C0
    uint16_t    io_count;
    uint16_t    count_t1;
    uint16_t    count_t2;
    uint8_t     VESAMajor;                   // VESA version major
    uint8_t     VESAMinor;                   // VESA version minor
    uint16_t    VESATotalMem;                // VESA memory chunks
} realmode_data;
#pragma pack (pop)

#endif  // RMDATA_H
