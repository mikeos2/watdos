/****************************************************************************
 *
 *  boot.h -- loader -- linux module defines and functions
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mike@mgreene.org>
 *                      June 2008
 * 
 *    Version 1.1       Michael K Greene <mikeos2@gmail.com>
 *                      May 2019
 *
 *  ========================================================================
 *
 *  Description: Header contains misc functions and defines required by
 *               modified linux modules.
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef BOOT_BOOT_H
#define BOOT_BOOT_H


//#include <bios.h>
#include <stdint.h>
//#include <stdarg.h>
//#include <stddef.h>


//#ifndef _COMDEF_H_INCLUDED
// #include <_comdef.h>
//#endif

#define _1K      (1L*1024L)     /* 1 k */
#define _64K     (64L*_1K)     /* 64 k */
#define _1MEG    (_1K*_1K)    /* 1 m */
#define _16MEG   (16L*_1MEG)   /* 16 m */

#define ORD_DOSIODELAYCNT 427

// constants for significant memory values
#define _1KB      (1L*1024L)     /* 1 k */
#define _64KB     (64L*_1KB)     /* 64 k */
#define _128KB    (2L*_64KB)     /* 128 k */
#define _640KB    (10L*_64KB)    /* 640 k */
#define _1MEGB    (_1KB*_1KB)    /* 1 m */
#define _4MEGB    (4L*_1MEGB)    /* 4 m */
#define _16MEGB   (16L*_1MEGB)   /* 16 m */
#define _64MEGB   (64L*_1MEGB)   /* 64 m */
#define _4GB      (0L)           /* 4 g (32 bit math wraps) */


#define CONFIG_X86_MINIMUM_CPU_FAMILY  6

// see OW header bios.h
#define COMPORTDEFAULTS  (_COM_9600 | _COM_NOPARITY | _COM_CHR8 | _COM_STOP1)

// *** from osfree freeldr
// Make physical address from far pointer
#define PHYS_FROM_FP(x) ((((unsigned long)(FP_SEG(x))) << 4) + (FP_OFF(x)))

// Make physical address from near pointer
#define PHYS_FROM_NP(seg, x) ((((unsigned long)(seg)) << 4) + (FP_OFF(x)))

// Make far pointer from physical address
#define FP_FROM_PHYS(x) (MK_FP( (((x) >> 4) & 0xFFFF), ((x) & 0xf)))


/* These functions are used to reference data in other segments. */

static inline uint16_t ds(void)
{
    uint16_t dsseg = 0;

    _asm {
        mov    ax, ds
        mov    dsseg, ax
    }
    return dsseg;
}

static inline void set_fs(uint16_t fsseg)
{
    _asm {
        mov    ax, fsseg
        mov    fs, ax
    }
}

static inline uint16_t fs(void)
{
    uint16_t fsseg = 0;

    _asm {
        mov    ax, fs
        mov    fsseg, ax
    }
    return fsseg;
}

static inline void set_gs(uint16_t gsseg)
{
    _asm {
        mov    ax, gsseg
        mov    gs, ax
    }
}

static inline uint16_t gs(void)
{
    uint16_t gsseg = 0;

    _asm {
        mov    ax, gs
        mov    gsseg, ax
    }
    return gsseg;
}

typedef unsigned int addr_t;

static inline uint8_t rdfs8(addr_t addr)
{
    uint8_t v = 0;
    uint8_t addrtmp = 0;

    addrtmp = (uint8_t)addr;

    _asm {
        mov   al, fs:addrtmp
        mov   v, al
    }
//asm volatile("movb %%fs:%1,%0" : "=r" (v) : "m" (*(u8 *)addr));

    return v;
}

static inline uint32_t rdfs32(addr_t addr)
{
    uint32_t v = 0;
    uint32_t addrtmp = 0;

    addrtmp = (uint32_t)addr;

    _asm {
        mov   eax, fs:addrtmp
        mov   v, eax
    }

//      asm volatile("movl %%fs:%1,%0" : "=r" (v) : "m" (*(u32 *)addr));
    return v;
}

static inline uint32_t rdgs32(addr_t addr)
{
    uint32_t v = 0;
    uint32_t addrtmp = 0;

    addrtmp = (uint32_t)addr;

    _asm {
        mov   eax, gs:addrtmp
        mov   v, eax
    }

//      asm volatile("movl %%fs:%1,%0" : "=r" (v) : "m" (*(u32 *)addr));
    return v;
}

static inline void wrfs32(uint32_t v, addr_t addr)
{
    uint32_t addrtmp = 0;

    addrtmp = (uint32_t)addr;

    _asm {
        mov   eax, fs:addrtmp
        mov   v, eax
    }
}

#endif /* BOOT_BOOT_H */

