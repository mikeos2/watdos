/****************************************************************************
 *
 *  pitdef.h -- PIT defines
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: 8254 Programmable Interval Timer
 * 
 *  Ref: http://www.brokenthorn.com/Resources/OSDevPit.html
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef PITDEF_H
#define PITDEF_H

/*
 * 1. Channel 0 is connected to the 8259 PIC to generate an Interrupt Request (IRQ). 
 *    The PITs OUT pin connects to the PIC's IR0 pin. Typically the BIOS configures 
 *    this channel with a count of 65536, which gives an output frequency of 18.2065 Hz. 
 *    This fires IRQ 0 every 54.9254 ms.
 * 
 * 2. Channel 1 video cards and the BIOS may reprogram the second channel for their own 
 *    uses. This channel was originally used for generating a timing pulse signal to 
 *    signal the memory controller to refresh the DRAM memory. There is no guarantee 
 *    what devices may use this counter.
 * 
 * 3. Channel 2 is connected to the PC Speaker to generate sounds
 */

/*
 * 8253 PIT Internal Registers
 * 
 * Register Name  Port Address  RD line  WR line  A0 line  A1 line  Function
 * Counter 0      0x40          1        0        0        0        Load  Counter 0
 *                              0        1        0        0        Reads Counter 0
 * Counter 1      0x41          1        0        0        1        Load  Counter 1
 *                              0        1        0        1        Reads Counter 1
 * Counter 2      0x42          1        0        1        0        Load  Counter 2
 *                              0        1        1        0        Reads Counter 2
 * Control Word   0x43          1        0        1        1        Write Control Word
 *     NA                       0        1        1        1        No operation
 */
 
#define PIT_CNT0_PORT      0x40
#define PIT_CNT1_PORT      0x41
#define PIT_CNT2_PORT      0x42
#define PIT_CTNL_PORT      0x43

#define PIT_CNT0_LOAD      0x08
#define PIT_CNT0_READ      0x04

#define PIT_CNT1_LOAD      0x09
#define PIT_CNT1_READ      0x05

#define PIT_CNT2_LOAD      0x0A
#define PIT_CNT2_READ      0x06

#define PIT_CTNL_WRITE     0x0B
#define PIT_CTNL_NOOP      0x07


// ******* Control Word Register ********

// Bit 0: (BCP) Binary Counter
#define PIT_BINARY         0x00        // 0: Binary
#define PIT_BCD            0x01        // 1: Binary Coded Decimal (BCD) */

// Bit 1-3: (M0, M1, M2) Operating Mode. 
#define PIT_MODE_0         (0x00 << 1) // 000: Mode 0: Interrupt or Terminal Count
#define PIT_MODE_1         (0x01 << 1) // 001: Mode 1: Programmable one-shot
#define PIT_MODE_2         (0x02 << 1) // 010: Mode 2: Rate Generator
#define PIT_MODE_3         (0x03 << 1) // 011: Mode 3: Square Wave Generator
#define PIT_MODE_4         (0x04 << 1) // 100: Mode 4: Software Triggered Strobe
#define PIT_MODE_5         (0x05 << 1) // 101: Mode 5: Hardware Triggered Strobe

// Bits 4-5: (RL0, RL1) Read/Load Mode.
#define PIT_RL_0           (0x00 << 4) // 00: Counter value is latched into an internal control register
#define PIT_RL_1           (0x01 << 4) // 01: Read or Load Least Significant Byte (LSB) only
#define PIT_RL_2           (0x02 << 4) // 10: Read or Load Most Significant Byte (MSB) only
#define PIT_RL_3           (0x03 << 4) // 11: Read or Load LSB first then MSB

// Bits 6-7: (SC0-SC1) Select Counter.
#define PIT_COUNTER_0      (0x00 << 6) // 00: Counter 0
#define PIT_COUNTER_1      (0x01 << 6) // 01: Counter 1
#define PIT_COUNTER_2      (0x02 << 6) // 10: Counter 2


#endif /* PITDEF_H */
