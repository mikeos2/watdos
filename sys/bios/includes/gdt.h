/****************************************************************************
 *
 *  gdt.h -- GDT defines
 *
 *  ========================================================================
 *
 *    Version 1.0       Michael K Greene <mikeos2@gmail.com>
 *                      July 2019
 *
 *  ========================================================================
 *
 *  Description: Global Descriptor Table
 * 
 *  Ref: http://www.brokenthorn.com/Resources/
 *
 *  ========================================================================
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 ***************************************************************************/

#ifndef GDTDEF_H
#define GDTDEF_H

#include <stdint.h>

/*
 * location of the GDT description structure for LGDT instruction
 * Intel Vol 3a p3-15
 *
 * ---- GDT 64 bits (8 bytes)
 *
 *             Bits   Function         Description
 * LimitLo --> 0-15   Limit 0:15       First 16 bits in the segment limiter
 * BaseLo  --> 16-31  Base 0:15        First 16 bits in the base address
 * BaseMid --> 0-7    Base 16:23       Bits 16-23 in the base address
 * Flags   --> 8-12   Type Segment     type and attributes
 *             13-14  Privilege Level  0 = Highest privilege (OS), 3 = Lowest privilege (User applications)
 *             15     Present flag     Set to 1 if segment is present
 * Attribs --> 16-19  Limit 16:19      Bits 16-19 in the segment limiter
 *             20-22  Attributes       Different attributes, depending on the segment type
 *             23     Granularity      Used together with the limiter, to determine the size of the segment
 * BaseHi  --> 24-31  Base 24:31       The last 24-31 bits in the base address
 * 
 * ---- Flags/Attribs defines 
 * 
 * Bits   uint8_t   Description
 * 8-12    0-4      Type Segment  
 * 13-14   5-6      Privilege     0 = Highest privilege (OS), 3 = Lowest privilege (User applications)
 * 15      7        Present       Set to 1 if segment is present
 * 
 * 16-19   0-3      Limit 16:19
 * 20-22   4-6      Attributes    Different attributes, depending on the segment type
 * 23      7        Granularity   0 = 1 byte, 1 = 1kbyte, pmode -- limit 4G
 * 
 * Segment Type
 * Bit     Name
 *  0   S  Descriptor type      This bit should be set for code or data segments and should be cleared for 
 *                                 system segments (eg. a Task State Segment)
 *  1   E  Executable           If 1 code in this segment can be executed, ie. a code selector. 
 *                              If 0 it is a data selector.
 *  2   DC                      Direction bit for data selectors: Tells the direction. 0 the segment grows up. 
 *                                       1 the segment grows down, ie. the offset has to be greater than the limit.
 *                              Conforming bit for code selectors:
 *                                       If 1 code in this segment can be executed from an equal or lower privilege level. 
 *                                       For example, code in ring 3 can far-jump to conforming code in a ring 2 segment. 
 *                                       The privl-bits represent the highest privilege level that is allowed to execute 
 *                                       the segment. For example, code in ring 0 cannot far-jump to a conforming code 
 *                                       segment with privl==0x2, while code in ring 2 and 3 can. Note that the privilege 
 *                                       level remains the same, ie. a far-jump form ring 3 to a privl==2-segment remains 
 *                                       in ring 3 after the jump.
 *                                       If 0 code in this segment can only be executed from the ring set in privl.
 *  3   RW Read/Write           Readable bit for code selectors: Whether read access for this segment is allowed. 
 *                                 Write access is never allowed for code segments.
 *                              Writable bit for data selectors: Whether write access for this segment is allowed. 
 *                                 Read access is always allowed for data segments.
 *  4   AC Accessed             Just set to 0. The CPU sets this to 1 when the segment is accessed.
 */
#define GDT_SEG_CODE                          0x01
#define GDT_SEG_DATA                          0x01
#define GDT_SEG_TSS                           0x00

#define GDT_SEG_NOEXECUTE                     (0x00 << 1)
#define GDT_SEG_EXECUTE                       (0x01 << 1)

// **** For data segments: GDT_SEG_CODE set ****
#define GDT_CODE_DC_SAMEPRIVL                 (0x00 << 2)   // code in this segment only be executed from the ring set in privl
#define GDT_CODE_DC_EQUALPRIVL                (0x01 << 2)   // code in this segment executed from an equal or lower privilege level

#define GDT_CODE_NOREAD                       (0x00 << 3)   
#define GDT_CODE_READ                         (0x01 << 3)   // code segment readable <-- uaually this
// end if GDT_SEG_CODE set

// **** For data segments: GDT_SEG_DATA set ****
#define GDT_DATA_DC_GROWSUP                   (0x00 << 2)   // DC: segment grows up  <-- uaually this
#define GDT_DATA_DC_GROWSDOWN                 (0x01 << 2)   // DC: segment grows up

#define GDT_DATA_NOWRITE                      (0x00 << 3)   // data seg writable/not writable
#define GDT_DATA_WRITE                        (0x01 << 3)   //                       <-- uaually this
// end if GDT_SEG_DATA set
 
#define GDT_ACCESSED                          (0x00 << 4)   // <-- just set this to zero

#define GDT_FLAG_DPL_RING0                    (0x00 << 5)   // 0 = Highest privilege (OS), 
#define GDT_FLAG_DPL_RING1                    (0x01 << 5)   // 3 = Lowest privilege (User applications)
#define GDT_FLAG_DPL_RING2                    (0x02 << 5)   
#define GDT_FLAG_DPL_RING3                    (0x03 << 5)   

#define GDT_FLAG_NOTPRESENT                   (0x00 << 7)   // Set to 1 if segment is present
#define GDT_FLAG_PRESENT                      (0x01 << 7)   //               <-- uaually this
 
/* Example segment Flags
 * BIT:        0               1                    2                    3          
 * CODE:  GDT_SEG_CODE | GDT_SEG_EXECUTE | GDT_CODE_DC_SAMEPRIVL | GDT_CODE_READ | 
 *        
 *              4               5-6                 7
 *        GDT_ACCESSED | GDT_FLAG_DPL_RING0 | GDT_FLAG_PRESENT
 */ 

/* Example segment Attribs
 * Bits           0-3             4-6                7           
 * Flags =    Limit 16:19 | GDT_FLAG_SIZE | GDT_FLAG_GRANULARITY
 */
#define GDT_FLAG_SIZE16                       (0x000 << 4)  // 16 bit protected mode
#define GDT_FLAG_SIZE32                       (0x100 << 4)  // 32 bit protected mode

#define GDT_FLAG_GRANULARITY1B                (0x00 << 7)   // 0 = 1 byte
#define GDT_FLAG_GRANULARITY4K                (0x01 << 7)   // 1 = 1kbyte

#pragma pack (push, 1)
struct gdt_ptr_struct {
    uint16_t limit;            // size of GDT
    uint32_t base;             // address start of GDT
};
#pragma pack (pop)
typedef struct gdt_ptr_struct gdt_ptr_t;

// GDT descriptor
#pragma pack (push, 1)
struct gdt_descriptor {
    uint16_t    LimitLo;      // First 16 bits in the segment limiter
	uint16_t    BaseLo;       // First 16 bits in the base address
	uint8_t     BaseMid;      // Bits 16-23 in the base address
	uint8_t     Flags;        // Flags and Bits 16-19 in the segment limiter
	uint8_t     Attribs;
	uint8_t     BaseHi;       // The last 24-31 bits in the base address
};
#pragma pack (pop)
typedef struct gdt_descriptor gdt_desc_t;

void gdt_init(uint16_t limit, uint32_t base);
uint32_t GDT_get_base( void);

#endif /* GDTDEF_H */
